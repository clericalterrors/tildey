# Contributing

Thank you for considering to contribute to Tildey! Please read through this document so you can get a lay of the land and a good start. If you have any questions or feedback feel free to [create an issue](https://gitlab.com/tildey/tildey/new).

## Prequisites

- [Git](https://git-scm.com/)
- [NodeJS](https://nodejs.org/en/download/) (LTS is recommended)
- [Yarn](https://yarnpkg.com/en/docs/install)

## Doing The Thing

### Fork & Clone

As with any project, the first thing you'll want to do is [fork the project](https://gitlab.com/tildey/tildey/forks/new), creating a copy for yourself to work with. Afterwards, depending on whether you have [SSH keys setup on your account](https://docs.gitlab.com/ce/ssh/README.html) you can clone it…

- using SSH: `git clone git@gitlab.com:<username>/tildey.git`
- using HTTPS: `git clone https://gitlab.com/<username>/tildey.git`

Once done, `cd tildey` and run `yarn` to install the dependencies.

### Configuration

After cloning, you'll want to create a `.env` file using the [`.env.example`](.env.example). You'll need to create [a Discord bot application](https://discordapp.com/developers/applications) and enter the bot's token as well as change the prefix to any of your chosing.

If you're working on the webhook integration, you'll need to create a `webhooks.json` in the project root and define it [like the Webhook interface](src/interfaces/webhook.ts) (an example can be found inside the interface file).

### Linting & Testing

For linting we use [XO](https://github.com/xojs/xo) with a TypeScript config and some rules altered (see [package.json](package.json)). You can run this with `yarn xo`.

For testing we use [Ava](https://github.com/avajs/ava) as the test runner and [nyc](https://github.com/istanbuljs/nyc) for coverage, all tests are located inside `test/`. You can run this with `yarn nyc ava`.

To run both linting and testing easily, you can use `yarn test`.

### Committing

To build and run Tildey, you can use `yarn start` which will launch Nodemon and watch the TypeScript for any changes. For production we use [PM2](https://pm2.keymetrics.io/), the `yarn production` script will compile the TypeScript and then run PM2. Any logs Tildey creates are output to `logs/combined.log` and `logs/error.log`. Note that in development logs will be output to the console as well as the log files, while in production they will only be output to the log files.

Now that you're ready to run Tildey, [pick an issue](https://gitlab.com/tildey/tildey/issues?state=opened) and get to work! Please comment on the issue that you've started on it to avoid multiple people working on the same thing, thank you.

### Pushing

When you run `git push`, [Husky](https://github.com/typicode/husky) is going to run `yarn test` before allowing you to push making sure the linting and tests pass. If they're not passing or you would just like to skip them, add `--no-verify` to your `git push`.

### Syncing

If you forked the project and cloned it from your account, you'll need a way to retrieve the new commits from the main repository. To do this you'll want to add an "upstream" remote:

- using SSH: `git remote add upstream git@gitlab.com:tildey/tildey.git`
- using HTTPS: `git remote add upstream https://gitlab.com/tildey/tildey.git`

And then whenever you want to sync up the changes: checkout into the master branch, fetch, and merge (and push the new changes to your repository if you want). Like so:

```sh
git checkout master
git fetch upstream master
git merge upstream/master master
git push origin master
```

You can view all your remotes using `git remote -v`.
