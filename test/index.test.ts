// Define AVA Macros and extra data here instead of in each file, sometimes we'll wanna re-use them

import { resolve } from 'path';
import { ExecutionContext, Macro } from 'ava';
import { writeFileSync } from 'fs-extra';
import { initEnv } from '../src/utils/env';

// Standard "input equals expected" Macro
export const isEqual: Macro<[any, any]> =
(t: ExecutionContext, input: any, expected: any): void => {
  // If we're passing through a function, run it to get the result
  if (typeof input === 'function') {
    input = input();
  }

  if (typeof expected === 'function') {
    expected = expected();
  }

  t.is(input, expected);
};

// Writes .env.test and tests that `initEnv()` will throw the correct error
export const envThrows: Macro<[string, string]> =
(t: ExecutionContext, env: string, error: string): void => {
  const envPath: string = resolve('.env.test');
  writeFileSync(envPath, env);
  t.throws(
    () => initEnv(envPath),
    error
  );
};
