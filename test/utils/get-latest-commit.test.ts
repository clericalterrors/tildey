import test from 'ava';
import simpleGit from 'simple-git/promise';
import { DefaultLogFields } from 'simple-git/typings/response';
import { getLatestCommit } from '../../src/utils';
import { isEqual } from '../index.test';

let input: DefaultLogFields;
let expected: DefaultLogFields;
test.before(async (): Promise<void> => {
  input = await getLatestCommit();
  expected = await simpleGit().log(['-1']).then((commit) => {
    return commit.latest;
  });
});

test('getLatestCommit returns the latest commit', isEqual, input!, expected!);
