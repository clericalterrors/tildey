import test from 'ava';
import { parseFuzzyTimeOffset } from '../../src/utils';
import { isEqual } from '../index.test';

const fuzzyTimeTests: Array<{ title: string; input: string; maxDays: number; expected: number }> = [
  {
    title: 'parseFuzzyTimeOffset returns valid days time',
    input: '1d', maxDays: 10, expected: 86400 * 1000
  },
  {
    title: 'parseFuzzyTimeOffset returns valid hours time',
    input: '1h', maxDays: 10, expected: 3600 * 1000
  },
  {
    title: 'parseFuzzyTimeOffset returns valid minutes time',
    input: '1m', maxDays: 10, expected: 60 * 1000
  },
  {
    title: 'parseFuzzyTimeOffset returns valid seconds time',
    input: '10s', maxDays: 10, expected: 10 * 1000
  },
  {
    title: 'parseFuzzyTimeOffset returns 5s if seconds specified is less than 5',
    input: '1s', maxDays: 10, expected: 5 * 1000
  },
  {
    title: 'parseFuzzyTimeOffset returns 0 if calculated time is greater than maxDays passed',
    input: '1d1h', maxDays: 1, expected: 0
  }
];

fuzzyTimeTests.map(({ title, input, maxDays, expected }) =>
  test(
    title, isEqual,
    () => parseFuzzyTimeOffset(input, maxDays),
    expected
  )
);
