import test from 'ava';
import { prefixes } from '../../src/utils';
import { isEqual } from '../index.test';

test(
  'prefixes is exported properly', isEqual,
  prefixes.length, 0
);
