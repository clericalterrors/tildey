import test from 'ava';
import { Message } from 'discord.js';
import { getPrefix, prefixes } from '../../src/utils';
import { isEqual } from '../index.test';

const messageNoGuild: Message = {} as unknown as Message;

const messageNoPrefix: Message = {
  guild: {
    id: '0'
  }
} as unknown as Message;

const messageWithPrefix: Message = {
  guild: {
    id: '1'
  }
} as unknown as Message;

test.before(() => {
  prefixes.push({
    guild: '1',
    prefix: 'test'
  });
});

test(
  'getPrefix returns default prefix if msg.guild doesn\'t exist', isEqual,
  () => getPrefix(messageNoGuild),
  process.env.TLDY_PREFIX
);

test(
  'getPrefix returns default prefix if msg.guild exist but prefixes doesn\'t have a known prefix', isEqual,
  () => getPrefix(messageNoPrefix),
  process.env.TLDY_PREFIX
);

test(
  'getPrefix returns changed prefix', isEqual,
  () => getPrefix(messageWithPrefix),
  'test'
);
