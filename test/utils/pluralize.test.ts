import test from 'ava';
import { pluralize } from '../../src/utils';
import { isEqual } from '../index.test';

test(
  'pluralize returns without postfix for count 1', isEqual,
  () => pluralize('test', 's', 1), '1 test'
);

test(
  'pluralize returns with postfix for count >1', isEqual,
  () => pluralize('test', 's', 2), '2 tests'
);
