import test from 'ava';
import { Emoji } from 'discord.js';
import { CustomEmojis, getCustomEmoji } from '../../src/utils/custom-emoji';
import { isEqual } from '../index.test';

// Before the tests run add the tildey emoji to the Collection
test.before(() => {
  // This is a weird way of setting a string as an Emoji in a Collection
  // but it lets us do the test properly without needing to use Discord directly
  CustomEmojis.set('tildey', '<:tildey:511947450735394817>' as unknown as Emoji);
});

// Define the tests to use with the isEqual Macro
const emojiTests: Array<{ title: string; input: () => any; expected: string }> = [
  {
    title: 'getCustomEmoji returns correct emoji',
    input: () => getCustomEmoji('tildey'),
    expected: '<:tildey:511947450735394817>'
  },
  {
    title: 'getCustomEmoji returns white large square for non-existent emoji',
    input: () => getCustomEmoji('test'),
    expected: ':white_large_square:'
  }
];

emojiTests.map(({ title, input, expected }) => test(title, isEqual, input, expected));
