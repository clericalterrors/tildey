import test from 'ava';
import { Command, Commands } from '../../src/commands';
import { getCommandsFromCategory } from '../../src/utils/get-commands-from-category';
import { isEqual } from '../index.test';

// Before the tests run add a dummy Command
test.before(() => {
  Commands.set('test', new Command({
    name: 'Test',
    aliases: ['test'],
    args: [],
    authorPermissions: null!,
    clientPermissions: null!,
    category: 'test',
    description: 'test',
    guildOnly: false,
    requiresDatabase: false,
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    execute: async (): Promise<void> => {}
  }));
});

// Define the tests to use with the isEqual Macro
const categoryTests: Array<{ title: string; input: () => number; expected: number }> = [
  {
    title: 'getCommandsFromCategory returns Commands from existing category',
    input: () => getCommandsFromCategory('test').length,
    expected: 1
  },
  {
    title: 'getCommandsFromCategory returns empty array from non-existing category',
    input: () => getCommandsFromCategory('category').length,
    expected: 0
  }
];

categoryTests.map(({ title, input, expected }) => test(title, isEqual, input, expected));
