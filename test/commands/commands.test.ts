import { resolve } from 'path';
import test from 'ava';
import { readJSONSync } from 'fs-extra';
import { initCommands, Commands } from '../../src/commands';
import { isEqual } from '../index.test';

test(
  'initCommands adds commands from commands.json', isEqual,
  () => {
    initCommands();
    return Commands.array().length;
  },
  () => aliasedCommandsLength()
);

test(
  'initCommands adds debug command in development', isEqual,
  () => {
    process.env.TLDY_ENV = 'development';
    initCommands();
    return Commands.array().length;
  },
  () => aliasedCommandsLength() + 2
);

function aliasedCommandsLength(): number {
  let total = 0;
  for (const command of readJSONSync(resolve('commands.json'))) {
    total += command.aliases.length;
  }

  return total;
}
