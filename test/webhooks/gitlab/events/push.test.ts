import { join } from 'path';
import test, { ExecutionContext, Macro } from 'ava';
import { RichEmbed } from 'discord.js';
import { readJSON } from 'fs-extra';
import { formatPush } from '../../../../src/webhooks/gitlab/events';

const macro: Macro<[string]> = async (t: ExecutionContext, dataPath: string): Promise<void> => {
  const embed: RichEmbed = formatPush(await readJSON(join(__dirname, dataPath)));
  // Remove the current date from the footer otherwise the snapshot will never pass
  embed.setFooter(embed.footer?.text?.slice(0, embed.footer.text.indexOf(' — ')));
  t.snapshot(embed);
};

test(
  'format push returns with 1 commit',
  macro,
  'data/push-1-commit.json'
);

test(
  'format push returns with multiple commits',
  macro,
  'data/push-multiple-commits.json'
);
