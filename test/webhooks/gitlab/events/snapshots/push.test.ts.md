# Snapshot report for `test/webhooks/gitlab/events/push.test.ts`

The actual snapshot is saved in `push.test.ts.snap`.

Generated by [AVA](https://ava.li).

## format push returns with 1 commit

> Snapshot 1

    RichEmbed {
      author: undefined,
      color: 9169405,
      description: `[Bauke](https://gitlab.com/Bauke) pushed 1 commit to [master](https://gitlab.com/tildey/tildey/tree/788919c2e185c84c1daebcb1d3d460aca429586f).␊
      [Compare the changes](https://gitlab.com/tildey/tildey/compare/f458e8c427261d6653feac28c9fdab1d609c0006...788919c2e185c84c1daebcb1d3d460aca429586f) from `f458e8c4` to `788919c2`.␊
      - [`788919c2`](https://gitlab.com/tildey/tildey/commit/788919c2e185c84c1daebcb1d3d460aca429586f) chore: bump version (1.4.0) and update c…␊
      `,
      fields: [],
      file: undefined,
      files: [],
      footer: {
        icon_url: undefined,
        text: 'Push',
      },
      image: undefined,
      thumbnail: {
        url: 'https://secure.gravatar.com/avatar/ecd836ee843ff0ab75d4720bd40c2baf?s=80&d=identicon',
      },
      timestamp: undefined,
      title: ':white_large_square: GitLab',
      url: undefined,
    }

## format push returns with multiple commits

> Snapshot 1

    RichEmbed {
      author: undefined,
      color: 9169405,
      description: `[Bauke](https://gitlab.com/Bauke) pushed 2 commits to [master](https://gitlab.com/tildey/tildey/tree/f458e8c427261d6653feac28c9fdab1d609c0006).␊
      [Compare the changes](https://gitlab.com/tildey/tildey/compare/c7a29c0a319fdf406f03862fb66f600e39613a26...f458e8c427261d6653feac28c9fdab1d609c0006) from `c7a29c0a` to `f458e8c4`.␊
      - [`c32dba63`](https://gitlab.com/tildey/tildey/commit/c32dba63c3a711c7e31300b7398e5caf9a3240c7) chore: implement basic level and label c…␊
      - [`f458e8c4`](https://gitlab.com/tildey/tildey/commit/f458e8c427261d6653feac28c9fdab1d609c0006) chore␊
      `,
      fields: [],
      file: undefined,
      files: [],
      footer: {
        icon_url: undefined,
        text: 'Push',
      },
      image: undefined,
      thumbnail: {
        url: 'https://secure.gravatar.com/avatar/ecd836ee843ff0ab75d4720bd40c2baf?s=80&d=identicon',
      },
      timestamp: undefined,
      title: ':white_large_square: GitLab',
      url: undefined,
    }
