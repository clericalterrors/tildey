import { join } from 'path';
import test, { ExecutionContext, Macro } from 'ava';
import { RichEmbed } from 'discord.js';
import { readJSON } from 'fs-extra';
import { formatMergeRequest } from '../../../../src/webhooks/gitlab/events';

const macro: Macro<[string]> = async (t: ExecutionContext, dataPath: string): Promise<void> => {
  const embed: RichEmbed = formatMergeRequest(await readJSON(join(__dirname, dataPath)));
  // Remove the current date from the footer otherwise the snapshot will never pass
  embed.setFooter(embed.footer?.text?.slice(0, embed.footer.text.indexOf(' — ')));
  t.snapshot(embed);
};

test(
  'format merge request returns opened with a description',
  macro,
  'data/merge-request-opened.json'
);

test(
  'format merge request returns closed with a description',
  macro,
  'data/merge-request-closed.json'
);

test(
  'format merge request returns without a description',
  macro,
  'data/merge-request-no-description.json'
);

test(
  'format merge request returns with a description over 1000 characters',
  macro,
  'data/merge-request-1000-characters.json'
);
