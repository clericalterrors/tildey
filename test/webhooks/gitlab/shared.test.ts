import test, { ExecutionContext } from 'ava';
import { RichEmbed } from 'discord.js';
import fecha from 'fecha';
import { transports } from 'winston';
import { getAuthorLink, newGitlabEmbed } from '../../../src/webhooks/gitlab/shared';
import { isEqual } from '../../index.test';
import { log } from '../../../src/utils';

// Disable camelcase as GitLab's API uses it for data
/* eslint-disable @typescript-eslint/camelcase */
const sampleData1: {project: { web_url: string }; user_name: string} = {
  project: {
    web_url: 'https://gitlab.com/Tildey/tildey'
  },
  user_name: 'Bauke'
};

const sampleData2: {project: { web_url: string }; user: { name: string; username: string }} = {
  project: {
    web_url: 'https://gitlab.com/Tildey/tildey'
  },
  user: {
    name: 'Bauke',
    username: 'Bauke'
  }
};

const sampleData3: {user_avatar: string} = {
  user_avatar: 'https://example.com'
};

const sampleData4: {repository: {homepage: string}; user: {name: string; username: string}} = {
  repository: {
    homepage: 'https://gitlab.com/Tildey/tildey'
  },
  user: {
    name: 'Bauke',
    username: 'Bauke'
  }
};
/* eslint-enable */

test(
  'getAuthorLink returns proper link using user_name', isEqual,
  () => getAuthorLink(sampleData1),
  'https://gitlab.com/Bauke'
);

test(
  'getAuthorLink returns proper link using user.name', isEqual,
  () => getAuthorLink(sampleData2),
  'https://gitlab.com/Bauke'
);

test('newGitlabEmbed returns specified RichEmbed', (t: ExecutionContext) => {
  // Makes it so it doesn't log anything
  log.add(new transports.Console());
  log.level = 'error';
  t.deepEqual(
    newGitlabEmbed('Test', '#123456', sampleData3),
    new RichEmbed()
      .setColor('#123456')
      .setFooter(`Test — ${fecha.format(new Date(), 'YYYY-MM-DD HH:mm')}`)
      .setThumbnail('https://example.com')
      .setTitle(':white_large_square: GitLab')
  );
});

test(
  'getAuthorLink returns proper link with repository structure', isEqual,
  () => getAuthorLink(sampleData4),
  'https://gitlab.com/Bauke'
);
