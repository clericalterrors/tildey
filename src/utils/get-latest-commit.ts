import simpleGit from 'simple-git/promise';
import { DefaultLogFields } from 'simple-git/typings/response';

export async function getLatestCommit(): Promise<DefaultLogFields> {
  const { latest } = await simpleGit().log(['-1']);
  return latest;
}
