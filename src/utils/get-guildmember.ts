import { GuildMember, Message } from 'discord.js';

/**
 * @function getGuildMember
 * @description Returns a the user object of a specified user ID, mention or tag
 * @param msg The message to operate on
 * @param userID the ID of the User, can be in the form of a ping or just their tag
 * @param silent (Optional) Disable output on error
 * @returns {GuildMember} GuildMember object or undefined, in case none was found
 */
export function getGuildMember(msg: Message, userID: string, silent?: boolean): GuildMember | undefined {
  const { members } = msg.guild;
  const user: GuildMember | undefined = (members.get(userID.replace(/[\\<@!>]/g, '')) ??
    members.find((member) => member.user.tag === userID.replace('/@/g', ''))) || undefined;

  // Ignore the replying part since we can't mock this in testing
  /* istanbul ignore next */
  if (!silent && user === undefined) {
    msg.reply('there\'s no user by that name in this server!~ :shrug:');
  }

  return user;
}
