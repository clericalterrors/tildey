import { resolve } from 'path';
import { readJSONSync } from 'fs-extra';

// resolve() starts from the package root (`tildey/`) so we use that to get the package.json
export const { version } = readJSONSync(resolve('package.json'));
