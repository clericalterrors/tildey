import { DMChannel, GuildMember, Message, User } from 'discord.js';

/**
 * @function getUser
 * @description Returns a the user object of a specified user ID, mention or tag
 * @param msg The message to operate on
 * @param userID the ID of the User, can be in the form of a ping or just their tag
 * @param silent (Optional) Disable output on error
 * @returns {User} The User object or undefined, if none was found
 */
export function getUser(msg: Message, userID: string, silent?: boolean): User | undefined {
  const { channel, client, guild } = msg;
  let user: User | undefined;
  let member: GuildMember | undefined;

  switch (channel.type) {
    case 'text':
      member = (guild.members.get(userID.replace(/[\\<@!>]/g, '')) ??
        guild.members.find((member) => member.user.tag === userID.replace('/@/g', ''))) || undefined;
      user = member === undefined ? client.users.get(userID.replace(/\D/g, '')) ?? undefined : member.user;
      // Ignore the replying part since we can't mock this in testing
      /* istanbul ignore next */
      if (!silent && user === undefined) {
        msg.reply(`there's no user with the name \`${userID}\` in this server!~ :shrug:`);
      }

      break;
    case 'dm':
      user = (channel as DMChannel).recipient;
      break;
    default:
      return undefined;
  }

  return user;
}
