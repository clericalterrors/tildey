import { Format } from 'logform';
import * as rfs from 'rotating-file-stream';
import { createLogger, format, Logger, transports } from 'winston';

// Create the logger and export it without anything set
export const log: Logger = createLogger();

/**
 * @function initLog
 * @description Initializes the Winston logger
 * @returns {void} Nothing
 */
export function initLog(): void {
  // Create a custom print: `YYYY-MM-DD HH:mm:ss,SSS>LEVEL log message`
  const customPrint: Format = format.printf(/* istanbul ignore next */({ level, message, timestamp }) => {
    return `${timestamp} ${level.toUpperCase().padEnd(5)} ${message}`;
  });

  // Take the print we just made and combine it with timestamp
  const customFormat: Format = format.combine(
    format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss,SSS' }),
    format.simple(),
    customPrint
  );

  // Set the level and format accordingly
  log.level = 'info';
  log.format = customFormat;

  if (process.env.TLDY_ENV === 'development') {
    // Set the level to silly in development and add the Console transport
    log.level = 'silly';
    // In production we'll just want the logs to be in files so the Console
    // transport isn't needed then
    log.add(new transports.Console());

    // Log each level once to be sure it works, uncomment if needed
    // log.error('Test error log.');
    // log.warn('Test warn log.');
    // log.info('Test info log.');
    // log.debug('Test debug log.');
    // log.silly('Test silly log.');
  }

  // Keep 10 files with interval of 6 days (max 60 days of logs) or 10MB between rotation and compress to gzip
  const rfsOptions: any = {
    compress: 'gzip',
    interval: '6d',
    maxFiles: 10,
    path: 'logs/',
    size: '10M'
  };
  log.add(new transports.Stream({ stream: rfs.createStream('error.log', rfsOptions), level: 'error' }));
  log.add(new transports.Stream({ stream: rfs.createStream('combined.log', rfsOptions) }));
}
