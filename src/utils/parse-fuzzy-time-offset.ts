/**
 * @function parseFuzzyTimeOffset
 * @description Parses a time offset in a human-writable format to ms
 * @argument input The offset string
 * @argument maxDays The maximum amount time this function can return in days, higher values of `input` return 0
 */
export function parseFuzzyTimeOffset(input: string, maxDays: number): number {
  // Convert a time offset from the format "1d1h1m" to milliseconds
  const daysArray: RegExpMatchArray | null = /\d+d/i.exec(input);
  const hoursArray: RegExpMatchArray | null = /\d+h/i.exec(input);
  const minutesArray: RegExpMatchArray | null = /\d+m/i.exec(input);
  const secondsArray: RegExpMatchArray | null = /\d+s/i.exec(input);
  let days = 0;
  let hours = 0;
  let minutes = 0;
  let seconds = 0;
  if (daysArray !== null) {
    days = parseInt(daysArray[0].slice(0, daysArray[0].length - 1), 10);
  }

  if (hoursArray !== null) {
    hours = parseInt(hoursArray[0].slice(0, hoursArray[0].length - 1), 10);
  }

  if (minutesArray !== null) {
    minutes = parseInt(minutesArray[0].slice(0, minutesArray[0].length - 1), 10);
  }

  if (secondsArray !== null) {
    seconds = parseInt(secondsArray[0].slice(0, secondsArray[0].length - 1), 10);
    if (seconds < 5 && days + hours + minutes === 0) {
      seconds = 5;
    }
  }

  // This multiplies days, hours, minutes and seconds by the amount of seconds in them and adds them up.
  // The result is multiplied with 1000 to get ms.
  const result: number = ((days * 86400) + (hours * 3600) + (minutes * 60) + (seconds)) * 1000;
  const maximum: number = 86400 * 1000 * maxDays;
  // Always return a usable number
  return (isNaN(result) || result > maximum ? 0 : result);
}
