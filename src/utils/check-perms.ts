import { Permissions, PermissionString } from 'discord.js';

/**
 * @function checkPermissions
 * @description Tests whether certain Permissions matches other Permissions
 * @param {Permissions} input Permissions to test
 * @param {Permissions} match Permissions the input must match
 * @returns {boolean} Boolean indicating if Permissions are good or not
 */
export function checkPermissions(input: Permissions, match: Permissions): boolean {
  const _perms: PermissionString[] = input.toArray();
  // If the input is an Admin just return true
  if (input.toArray().includes('ADMINISTRATOR')) {
    return true;
  }

  return match.toArray().every((perm) => _perms.includes(perm));
}
