// Can't test this because it requires a connection to Discord
/* istanbul ignore file */

import { Client } from 'discord.js';
import { pluralize } from '.';

/**
 * @function updatePresence
 * @description Updates the Presence of the client to "Playing on X guild(s)"
 * @param client The client to update the presence of
 */
export async function updatePresence(client: Client): Promise<void> {
  await client.user.setPresence({
    game: {
      name: `on ${pluralize('guild', 's', client.guilds.size)}`
    },
    status: 'online'
  });
}
