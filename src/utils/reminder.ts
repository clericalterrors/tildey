import { Client, TextChannel } from 'discord.js';
import { EntityManager, getManager } from 'typeorm';
import { Reminder } from '../db';
import { log } from './log';

export const latestReminder = {
  handle: 0 as any
};

/* istanbul ignore next */
export async function nextReminder(client: Client, latestHandle: number): Promise<void> {
  const manager: EntityManager = getManager();
  const reminders: Reminder[] = await manager.find(
    Reminder,
    {
      order: {
        due: 'ASC'
      },
      take: 1
    }
  );

  clearTimeout(latestHandle);
  if (reminders.length === 0) {
    latestReminder.handle = 0;
    return;
  }

  const timeout: number = new Date(reminders[0].due).getTime() - Date.now();
  log.debug(`[NEXTREMINDER] Next reminder will fire in ${timeout / 1000}s`);
  let handle: any = 0;
  if (timeout > 60 * 60 * 1000) {
    handle = setTimeout(() => {
      nextReminder(client, latestHandle);
    }, 60 * 60 * 1000);
  } else {
    handle = setTimeout(() => {
      executeReminder(client, reminders[0].channel, reminders[0].user, reminders[0].content);
    }, timeout);
  }

  latestReminder.handle = handle;
}

/* istanbul ignore next */
async function executeReminder(client: Client, channelID: string, targetUserID: string, text: string): Promise<void> {
  const channel: TextChannel = client.channels.get(channelID) as TextChannel;
  if (channel) {
    channel.send(`<@${targetUserID}>!~ ${text}`);
  } else {
    try {
      (await client.users.get(targetUserID)!.createDM())
        .sendMessage(`<@${targetUserID}>!~ ${text}`);
    } catch {
      // If it can't DM them they've either deleted their account or blocked the bot
    }
  }

  const manager: EntityManager = getManager();
  const reminders: Reminder[] = await manager.find(
    Reminder,
    {
      order: {
        due: 'ASC'
      },
      take: 1
    }
  );
  if (reminders.length === 0) {
    latestReminder.handle = 0;
    return;
  }

  await manager.delete(Reminder, { id: reminders[0].id });
  await nextReminder(client, latestReminder.handle);
}
