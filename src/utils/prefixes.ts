import { Client } from 'discord.js';
import { EntityManager, getManager } from 'typeorm';
import { db, GuildInfo } from '../db';
import { Prefix } from '../interfaces';

export const prefixes: Prefix[] = [];

/* istanbul ignore next */
export async function initPrefixes(client: Client): Promise<void> {
  if (db.enabled) {
    const manager: EntityManager = getManager();
    for (const guild of client.guilds.array()) {
      const guildInfo: GuildInfo | undefined = await manager.findOne(GuildInfo, {
        id: guild.id
      });
      if (guildInfo === undefined) {
        prefixes.push({
          guild: guild.id,
          prefix: process.env.TLDY_PREFIX!
        });
      } else {
        prefixes.push({
          guild: guild.id,
          prefix: guildInfo.prefix || process.env.TLDY_PREFIX!
        });
      }
    }
  } else {
    for (const guild of client.guilds.array()) {
      prefixes.push({
        guild: guild.id,
        prefix: process.env.TLDY_PREFIX!
      });
    }
  }
}
