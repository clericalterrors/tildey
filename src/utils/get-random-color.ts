/**
 * @function getRandomColor
 * @description Returns a random hex color
 * @returns {string} Returns a string with a random hex color
 */
export function getRandomColor(): string {
  let hexRandom = '#';
  for (let i = 0; i < 6; i++) {
    hexRandom += '0123456789ABCDEF'[Math.floor(Math.random() * 16)];
  }

  return hexRandom;
}
