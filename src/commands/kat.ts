/* istanbul ignore file */

import { Message } from 'discord.js';
import { getCustomEmoji } from '../utils';

export async function katCmd(msg: Message): Promise<void> {
  const katID = '<@371151824331210755>';
  const katMsg = `${getCustomEmoji('katlove')} ${katID}'s the prettiest and greatest person in the world!~`;
  await msg.channel.send(katMsg);
}
