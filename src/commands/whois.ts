/* istanbul ignore file */

import { Message, User, RichEmbed, Guild } from 'discord.js';
import fecha from 'fecha';
import { EntityManager, getManager } from 'typeorm';
import { getCustomEmoji, getUser } from '../utils';
import { WhoIs } from '../db';

export async function whoisCmd(msg: Message, name: string, args: string[]): Promise<void> {
  if (args.length === 0) {
    await msg.reply(`you forgot to include a user!~ ${getCustomEmoji('pwease')}`);
    return;
  }

  const manager: EntityManager = getManager();
  let optout: WhoIs | undefined = await manager.findOne(WhoIs, { id: msg.author.id });
  if (args[0].toLowerCase() === 'optout') {
    if (optout === undefined) {
      await manager.save(new WhoIs({ id: msg.author.id }));
      await msg.reply(`you've successfully opted out of the whois command!~ ${getCustomEmoji('tildey')}`);
      return;
    }

    await msg.reply(`you've already opted out of the whois command!~ ${getCustomEmoji('tildey')}`);
    return;
  }

  if (args[0].toLowerCase() === 'optin') {
    if (optout === undefined) {
      await msg.reply(`you're already opted in to the whois command!~ ${getCustomEmoji('tildey')}`);
      return;
    }

    await manager.delete(WhoIs, optout);
    await msg.reply(`you're now opted in again to the whois command!~ ${getCustomEmoji('tildey')}`);
    return;
  }

  const targetUser: User | undefined = getUser(msg, args[0], true);
  if (targetUser === undefined) {
    await msg.reply(`I couldn't find anyone going by \`${args[0]}\` in any servers!~ :shrug:`);
    return;
  }

  optout = await manager.findOne(WhoIs, { id: targetUser.id });
  const superOwners: string[] = process.env.TLDY_SUPERS!.split(',');
  if (optout && !superOwners.includes(msg.author.id)) {
    await msg.reply(`this person has opted out of the whois command!~ ${getCustomEmoji('tildey')}`);
    return;
  }

  const userGuilds: string[] = msg.client.guilds
    .filter((guild: Guild) => guild.members.has(targetUser.id))
    .map((guild: Guild) => guild.name);

  let description = `${targetUser.tag} is a ${targetUser.bot ? 'bot' : 'human'}.\n`;
  description += 'They have been seen on ';
  if (superOwners.includes(msg.author.id) || targetUser.id === msg.author.id) {
    description += `these servers: \`${userGuilds.join('`, `')}\`.\n`;
  } else {
    description += `${userGuilds.length} servers.\n`;
  }

  description += `Created their account on ${fecha.format(targetUser.createdAt, 'YYYY-MM-DD HH:mm:ss')}.\n`;
  if (superOwners.includes(targetUser.id)) {
    description += 'Has superowner privileges.\n';
  }

  if (optout) {
    description += 'They have opted out of the whois command.\n';
  }

  const embed: RichEmbed = new RichEmbed()
    .setTitle(`${targetUser.tag} (${targetUser.id})`)
    .setThumbnail(targetUser.displayAvatarURL)
    .setColor('#ff79c6')
    .setDescription(description);
  await msg.channel.send(embed);
}
