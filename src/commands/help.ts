/* istanbul ignore file */

import { Message, RichEmbed } from 'discord.js';
import { getCustomEmoji, getPrefix, getCommandsFromCategory } from '../utils';
import { Command } from './command';
import { Commands } from './commands';

export async function helpCmd(msg: Message, name: string, args: string[]): Promise<void> {
  const embed: RichEmbed = new RichEmbed()
    .setTitle(`${getCustomEmoji('tildey')} ${msg.client.user.username}'s Help Center!~`)
    .setColor('ff5555');

  const allCommands: Command[] = Commands.array();
  const categories: string[] = [];
  const prefix: string = getPrefix(msg);
  for (const command of allCommands) {
    if (typeof categories.find((val: string) => command.category === val) === 'undefined') {
      categories.push(command.category);
    }
  }

  if (args.length === 0) {
    // Iterate through command categories and add them to the embed
    let categoriesText = '';
    for (const category of categories) {
      categoriesText += `- ${category}\n`;
    }

    categoriesText += `Type \`${prefix}help <Category> \` for a list of all commands in that category.`;
    embed.addField('Categories', categoriesText);
    // Iterate through all commands and add 3 randomly selected ones and add them to the embed
    let commandsText = 'Listing 3 randomly selected commands:\n';
    for (let i = 0; i < 3; i++) {
      // Randomly selected a command's first alias to display, since some commands have multiples this is the easiest
      let randomCommandName: string = allCommands[Math.floor(Math.random() * allCommands.length)].aliases[0];
      // This while() makes sure there's no duplicates
      while (commandsText.includes(randomCommandName)) {
        randomCommandName = allCommands[Math.floor(Math.random() * allCommands.length)].aliases[0];
      }

      commandsText += `- ${randomCommandName}\n`;
    }

    commandsText += `Type \`${prefix}help <Command> \` for all the information of that command.`;
    embed.addField('Commands', commandsText);
    embed.addField('Website', 'To view all commands and categories more easily, check out [the website](https://tildey.xyz).');

    await msg.channel.send(embed);
    return;
  }

  // If the first argument passed is found inside the command categories, it's a "~help <Category>" request
  if (new RegExp(args[0], 'i').exec(categories.toString())) {
    let selectedCategory = '';
    for (const category of categories) {
      if (category.toLowerCase() === args[0].toLowerCase()) {
        selectedCategory = category;
      }
    }

    embed.setTitle(`${getCustomEmoji('tildey')} ${msg.client.user.username}'s ${selectedCategory}!~`);
    const categoryCommands: Command[] = [];
    for (const command of getCommandsFromCategory(selectedCategory)) {
      if (typeof categoryCommands.find((val: Command) => command.name === val.name) === 'undefined') {
        categoryCommands.push(command);
      }
    }

    for (const command of categoryCommands) {
      let commandInfo = '';
      commandInfo += `${command.description}\n`;
      commandInfo += 'Run with: ';
      for (const alias of command.aliases) {
        if (typeof command.args === 'undefined') {
          commandInfo += `\`${prefix}${alias}\` or `;
        } else {
          for (const arg of command.args) {
            if (arg.length > 1) {
              commandInfo += `\`${prefix}${alias} ${arg}\` or `;
            } else {
              commandInfo += `\`${prefix}${alias}\` or `;
            }
          }
        }
      }

      // Removes trailing " or " from aliases.
      commandInfo = commandInfo.slice(0, commandInfo.length - 4);
      embed.addField(command.name, commandInfo);
    }

    embed.addField(
      'Website',
      `This category can also be viewed on [the website](https://tildey.xyz/category/${selectedCategory.toLowerCase()}.html).`
    );
    await msg.channel.send(embed);
    return;
  }

  for (const command of allCommands) {
    // If the aliases includes the first argument passed it's a "~help <Command>" request
    if (command.aliases.includes(args[0].toLowerCase())) {
      // Set the title to the command's name
      embed.setTitle(`${getCustomEmoji('tildey')} ${msg.client.user.username}'s "${command.name}" Command!~`);
      let commandInfo = '';
      commandInfo += `${command.description}\n`;
      commandInfo += 'Run with: ';
      // Iterate through the command's aliases and list all the possibilities appropriately
      for (const alias of command.aliases) {
        if (typeof command.args === 'undefined') {
          commandInfo += `\`${prefix}${alias}\` | `;
        } else {
          for (const arg of command.args) {
            if (arg.length > 1) {
              // \u00A0 is a non-breaking space
              commandInfo += `\`${prefix}${alias}\u00A0${arg}\` | `;
            } else {
              commandInfo += `\`${prefix}${alias}\` | `;
            }
          }
        }
      }

      // Removes trailing " | " from the command aliases.
      commandInfo = commandInfo.slice(0, commandInfo.length - 3);
      embed.addField(command.name, commandInfo);
      embed.addField(
        'Website',
        `This command can also be viewed on [the website](https://tildey.xyz/command/${command.aliases[0].toLowerCase()}.html).`
      );
      await msg.channel.send(embed);
      return;
    }
  }

  await msg.reply(`I didn't find commands or categories with that name!~ ${getCustomEmoji('pwease')}`);
}
