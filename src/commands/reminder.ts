/* istanbul ignore file */

import { Message, RichEmbed, User } from 'discord.js';
import { EntityManager, getManager } from 'typeorm';
import { Reminder } from '../db';
import { getCustomEmoji, getUser, parseFuzzyTimeOffset, latestReminder, nextReminder } from '../utils';

export async function reminderCmd(msg: Message, name: string, args: string[]): Promise<void> {
  const manager: EntityManager = getManager();
  const subcommand: string | null = args[0] === undefined ? null : args[0].toLowerCase();
  if (subcommand === null) {
    await msg.reply(`you forgot to include arguments!~ ${getCustomEmoji('pwease')}`);
  } else if (subcommand === 'add') {
    if (args.length < 3) {
      await msg.reply(`you need to include at least a target user, duration, and message!~ ${getCustomEmoji('pwease')}`);
      return Promise.resolve();
    }

    let targetUserID = '';
    const targetUserString: string = args[1].toLowerCase();
    if (targetUserString === 'me') {
      targetUserID = msg.author.id;
    } else {
      const user: User | undefined = getUser(msg, targetUserString);
      if (user === undefined) {
        return Promise.resolve();
      }

      targetUserID = user.id;
    }

    let reminderOffset: number = parseFuzzyTimeOffset(args[2], 420);
    let parseError = false;
    if (reminderOffset <= 0) {
      const targetDate = new Date(args[2]);
      if (isNaN(targetDate.valueOf())) {
        parseError = true;
      } else {
        reminderOffset = targetDate.valueOf() - Date.now();
        if (reminderOffset < 0 || reminderOffset > 86400 * 1000 * 420) {
          // If in the past or over 420 days in the future
          parseError = true;
        }
      }

      if (parseError) {
        await msg.reply(`I didn't find any valid times in your message (the maximum is 420 days)!~ ${getCustomEmoji('pwease')}`);
        return Promise.resolve();
      }
    }

    const reminderMessage: string = args.slice(3).join(' ');
    await manager.save(new Reminder({
      channel: msg.channel.id,
      content: reminderMessage,
      due: new Date(Date.now() + reminderOffset),
      user: targetUserID
    }));
    await msg.react(getCustomEmoji('tildey'));
  } else if (subcommand === 'list') {
    const reminders: Reminder[] = await manager.find(Reminder, { user: msg.author.id });
    const embed = new RichEmbed()
      .setTitle(`Reminders for ${msg.author.tag}`)
      .setColor('#bd93f9');
    embed.description = '';
    for (const reminder of reminders) {
      const { content, due, id } = reminder;
      // Get the difference between the due date and now in milliseconds
      // Divide the difference by (1000 / 60 / 60) to convert it to hours
      embed.description += `\`#${id}\` ${content.length > 40 ? `${content.slice(0, 30)}…` : content} (${((due.valueOf() - Date.now()) / 1000 / 60 / 60).toFixed(2)}h left)\n`;
    }

    if (embed.description.length === 0) {
      embed.description = '**You have no pending reminders!~**';
    }

    await msg.channel.send(embed);
  } else if (subcommand === 'remove') {
    if (args.length === 0 || isNaN(parseInt(args[1], 10))) {
      await msg.reply(`you forgot to include a reminder ID to delete!~ ${getCustomEmoji('pwease')}`);
      return;
    }

    // Hacky solution that tries to find a reminder by the author first and then
    // deletes it after finding. Less than ideal since we should be able to just
    // delete it straight up, however TypeORM doesn't yet return the deleted
    // result, so we can't check whether or not anything was deleted.
    // https://github.com/typeorm/typeorm/issues/2415
    const reminder: Reminder | undefined = await manager.findOne(Reminder, {
      id: Number(args[1]),
      user: msg.author.id
    });
    if (reminder === undefined) {
      await msg.reply(`that doesn't seem to be one of your reminders!~ ${getCustomEmoji('pwease')}`);
      return;
    }

    await manager.delete(Reminder, { id: reminder.id });
    await msg.react(getCustomEmoji('tildey'));
  } else if (subcommand === 'purge') {
    await manager.delete(Reminder, { user: msg.author.id });
    await msg.react(getCustomEmoji('tildey'));
  } else {
    await msg.reply(`that's not how you use the quote command!~ ${getCustomEmoji('pwease')}`);
  }

  await nextReminder(msg.client, latestReminder.handle);
}
