/* istanbul ignore file */

import { Message, Permissions, RichEmbed } from 'discord.js';
import fecha from 'fecha';
import { EntityManager, getManager } from 'typeorm';
import { Quote } from '../db';
import { getCustomEmoji, getPrefix, pluralize, getRandomColor, checkPermissions, getGuildMember } from '../utils';

export async function quoteCmd(msg: Message, name: string, args: string[]): Promise<void> {
  const manager: EntityManager = getManager();
  const count: number = await manager.count(Quote, { guild: msg.guild.id });
  if (count === 0 && args[0]?.toLowerCase() !== 'add') {
    await msg.reply('there are no quotes from this server yet!~ :shrug:');
    return;
  }

  if (args.length === 0) {
    const chosen: Quote = await manager.findOne(
      Quote,
      {
        guild: msg.guild.id,
        id: Math.ceil(Math.random() * count)
      }
    ) as Quote;
    await msg.channel.send(renderQuote(msg, chosen));
  } else if (args[0].toLowerCase() === 'count') {
    await msg.reply(`there are currently \`${count}\` quotes!~ ${getCustomEmoji('tildey')}`);
  } else if (args.length >= 3 && args[0].toLowerCase() === 'add') {
    let quoteID: number = count + 1;
    if (count > 0) {
      const highestID: Quote[] = await manager.find(Quote, {
        order: {
          id: 'DESC'
        },
        take: 1,
        where: {
          guild: msg.guild.id
        }
      });
      quoteID = highestID[0].id + 1;
    }

    const quote: Quote = new Quote({
      author: args[1],
      color: getRandomColor(),
      content: args.splice(2, args.length).join(' '),
      creator: msg.author.id,
      guild: msg.guild.id,
      id: quoteID
    });

    await manager.save(quote);
    await msg.channel.send(renderQuote(msg, quote).setFooter(`Added with ID ${quote.id}  | Colour ${quote.color}`));
  } else if (args[0].toLowerCase() === 'remove' && !isNaN(parseInt(args[1], 10))) {
    if (!checkPermissions(getGuildMember(msg, msg.author.id, true)!.permissions, new Permissions('MANAGE_GUILD'))) {
      await msg.reply('you\'re not allowed to use that command!~ :no_entry_sign:');
      return;
    }

    const quote: Quote | undefined = await manager.findOne(Quote, {
      guild: msg.guild.id,
      id: parseInt(args[1], 10)
    });
    if (quote === undefined) {
      await msg.reply(`I couldn't find a quote with that ID!~ ${getCustomEmoji('pwease')}`);
      return;
    }

    await manager.delete(Quote, { iid: quote.iid });
    await msg.react(getCustomEmoji('tildey'));
  } else if (args[0].toLowerCase() === 'list' && !isNaN(parseInt(args[1], 10))) {
    let id = Number(args[1]) - 1;
    if (id < 0) {
      id = 0;
    }

    const quotes: Quote[] = await manager.find(Quote, {
      order: {
        id: 'ASC'
      },
      skip: id,
      take: 5,
      where: {
        guild: msg.guild.id
      }
    });

    if (quotes.length === 0) {
      await msg.reply(`I couldn't find any quotes with that ID or above!~ ${getCustomEmoji('pwease')}`);
      return;
    }

    const superOwners: string[] = process.env.TLDY_SUPERS!.split(',');
    const embed: RichEmbed = new RichEmbed().setColor('#f1fa8c');
    for (let i = 0; i < 5; i++) {
      if (quotes[i] === undefined) {
        continue;
      }

      let quoteContent: string = quotes[i].content;
      if (quoteContent.length >= 500) {
        quoteContent = quoteContent.slice(0, 499) + '[…]';
      }

      embed.addField(
        `ID ${quotes[i].id}`,
        `**“${quoteContent}”**\n— ${quotes[i].author} ` +
        `${fecha.format(new Date(quotes[i].date), 'YYYY-MM-DD HH:mm:ss')}` +
        `${superOwners.includes(msg.author.id) ? ` (Added by <@${quotes[i].creator}>)` : ''}` +
        ` | Colour ${quotes[i].color}`
      );
    }

    embed.setTitle(`Quotes from ID ${quotes[0].id} to ID ${quotes[quotes.length - 1].id}`);
    await msg.channel.send(embed);
  } else if ((args.length === 2 || args.length === 3) && args[0].toLowerCase() === 'list') {
    let id = Number(args[1]) - 1;
    if (id < 0) {
      id = 0;
    }

    const quotes: Quote[] = await manager.find(Quote, {
      guild: msg.guild.id
    });
    let quotesArray: Quote[] = quotes.filter((quote: Quote) => {
      if (/<@!?\d+>/.exec(args[1])) {
        return quote.author.replace(/\D/g, '') === args[1].replace(/\D/g, '');
      }

      return quote.author === args[1];
    });

    const actualAmountOfQuotes: number = quotesArray.length;
    if (args.length === 3 && parseInt(args[2], 10)) {
      const page: number = parseInt(args[2], 10);
      if (quotesArray.length < page * 5) {
        await msg.reply(`there aren't that many quotes from that user!~ ${getCustomEmoji('pwease')}`);
        return;
      }

      if (page < 0) {
        await msg.reply(`you can't specify a negative page!~ ${getCustomEmoji('pwease')}`);
        return;
      }

      quotesArray = quotesArray.slice(page * 5, (page + 1) * 5);
    } else {
      quotesArray = quotesArray.slice(0, 5);
    }

    const superOwners: string[] = process.env.TLDY_SUPERS!.split(',');
    const embed: RichEmbed = new RichEmbed().setColor('#ffb86c');
    for (const individualQuote of quotesArray) {
      let quoteContent: string = individualQuote.content;
      if (quoteContent.length >= 500) {
        quoteContent = quoteContent.slice(0, 499) + '[…]';
      }

      embed.addField(
        `ID ${individualQuote.id}`,
        `**“${quoteContent}”**\n— ${individualQuote.author} ` +
        `${fecha.format(new Date(individualQuote.date), 'YYYY-MM-DD HH:mm:ss')}` +
        `${superOwners.includes(msg.author.id) ? ` (Added by <@${individualQuote.creator}>)` : ''}` +
        ` | Colour ${individualQuote.color}`
      );
    }

    embed.setTitle(`This user has ${pluralize('quote', 's', actualAmountOfQuotes)}!~`);
    if (actualAmountOfQuotes > 5) {
      embed.setFooter(`Use ${getPrefix(msg)}quote list @User 0…${Math.floor(actualAmountOfQuotes / 5)} to access different pages.`);
    }

    await msg.channel.send(embed);
  } else if (args.length === 1 && !isNaN(parseInt(args[0], 10))) {
    const quote: Quote | undefined = await manager.findOne(Quote, {
      guild: msg.guild.id,
      id: Number(args[0])
    });

    if (quote === undefined) {
      await msg.reply(`I couldn't find a quote with that ID in this server!~ ${getCustomEmoji('pwease')}`);
      return;
    }

    await msg.channel.send(renderQuote(msg, quote));
  } else {
    await msg.reply(`that's not how you use the quote command!~ ${getCustomEmoji('pwease')}`);
  }
}

function renderQuote(msg: Message, quote: Quote): RichEmbed {
  const superOwners: string[] = process.env.TLDY_SUPERS!.split(',');
  const content = `> ${quote.content.replace(/\n/g, '\n> ')}`;
  const embed = new RichEmbed()
    .setColor(quote.color)
    .setDescription(
      `${content}\n — ${quote.author}` +
      `${superOwners.includes(msg.author.id) ? ` (Added by <@${quote.creator}>)` : ''}`
    )
    .setFooter(`Added ${fecha.format(new Date(quote.date), 'YYYY-MM-DD HH:mm:ss')} | ID ${quote.id} | Colour ${quote.color}`);
  return embed;
}
