/* istanbul ignore file */

import { Message } from 'discord.js';
import { getCustomEmoji } from '../utils';

export async function chooseCmd(msg: Message, name: string, args: string[]): Promise<void> {
  if (args.length > 0) {
    const list: string[] = args.join(' ').replace(/,\s/g, ',').trim().split(',');
    const selection = list[Math.floor(Math.random() * list.length)];
    await msg.channel.send(`:thinking: Hmm, I choose ${selection}!~`);
  } else {
    await msg.reply(`you forgot to include options to choose from!~ ${getCustomEmoji('tildey')}`);
  }
}
