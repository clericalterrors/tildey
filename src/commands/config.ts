/* istanbul ignore file */

import { Message } from 'discord.js';
import { getManager, EntityManager } from 'typeorm';
import { GuildInfo } from '../db';
import { Prefix } from '../interfaces';
import { prefixes, getCustomEmoji } from '../utils';
import { Command } from './command';
import { Commands } from './commands';

export async function configCmd(msg: Message, name: string, args: string[]): Promise<void> {
  const manager: EntityManager = getManager();
  let info: GuildInfo | undefined = await manager.findOne(GuildInfo, msg.guild.id);
  if (info === undefined) {
    info = new GuildInfo({
      disabledCommands: [],
      id: msg.guild.id,
      prefix: process.env.TLDY_PREFIX
    });
    await manager.save(info);
  }

  if (args.length >= 2) {
    const arg: string = args[0].toLowerCase();
    if (arg === 'prefix') {
      const newPrefix: string = args[1];
      const prefix: Prefix = prefixes.find((val: Prefix) => val.guild === msg.guild.id)!;
      prefix.prefix = newPrefix;
      info.prefix = newPrefix;
      await manager.save(info);
      await msg.react(getCustomEmoji('tildey'));
      return;
    }

    if (arg === 'enable') {
      const commandName: string = args[1].toLowerCase();
      const command: Command | undefined = Commands.get(commandName);
      if (typeof command === 'undefined') {
        await msg.reply(`I didn't find any commands with that name!~ ${getCustomEmoji('pwease')}`);
        return;
      }

      const disabled: string[] = info.disabledCommands;
      if (!disabled.includes(commandName)) {
        await msg.reply(`that command is already enabled!~ ${getCustomEmoji('tildey')}`);
        return;
      }

      for (const alias of command.aliases) {
        const index: number = disabled.indexOf(alias);
        if (index > -1) {
          disabled.splice(index, 1);
        }
      }

      info.disabledCommands = disabled;
      await manager.save(info);
      await msg.react(getCustomEmoji('tildey'));
      return;
    }

    if (arg === 'disable') {
      const commandName: string = args[1].toLowerCase();
      const command: Command | undefined = Commands.get(commandName);
      if (typeof command === 'undefined') {
        await msg.reply(`I didn't find any commands with that name!~ ${getCustomEmoji('pwease')}`);
        return;
      }

      const protectedCommands: string[] = [
        'about',
        'config',
        'feedback',
        'help',
        'ping',
        'pong',
        'super',
        'uptime'
      ];

      if (protectedCommands.includes(commandName)) {
        await msg.reply('that command can\'t be disabled!~ :no_entry_sign:');
        return;
      }

      const disabled: string[] = info.disabledCommands;
      if (disabled.includes(commandName)) {
        await msg.reply(`that command is already disabled!~ ${getCustomEmoji('tildey')}`);
        return;
      }

      for (const alias of command.aliases) {
        disabled.push(alias);
      }

      info.disabledCommands = disabled;
      await manager.save(info);
      await msg.react(getCustomEmoji('tildey'));
      return;
    }
  }

  await msg.reply(`that's not how you use the config command!~ ${getCustomEmoji('pwease')}`);
}
