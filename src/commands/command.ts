import { Message, Permissions } from 'discord.js';

export class Command {
  public name: string;
  public description: string;
  public aliases: string[];
  public category: string;
  public args: string[];
  public authorPermissions: Permissions;
  public clientPermissions: Permissions;
  public guildOnly: boolean;
  public requiresDatabase: boolean;
  public execute: (message: Message, name: string, args: string[]) => Promise<void>;

  constructor(opts: Command) {
    this.name = opts.name;
    this.description = opts.description;
    this.aliases = opts.aliases;
    this.category = opts.category;
    this.args = opts.args;
    this.authorPermissions = opts.authorPermissions;
    this.clientPermissions = opts.clientPermissions;
    this.guildOnly = opts.guildOnly;
    this.requiresDatabase = opts.requiresDatabase;
    this.execute = opts.execute;
  }
}
