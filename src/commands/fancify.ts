/* istanbul ignore file */

import { Message } from 'discord.js';
import fancify from 'fancify';
import { getCustomEmoji } from '../utils';

export async function fancifyCmd(msg: Message, name: string, args: string[]): Promise<void> {
  if (args.length === 0) {
    await msg.reply(`you forgot to include a message to fancify!~ ${getCustomEmoji('pwease')}`);
    return;
  }

  // Typed as any[] because fancify doesn't use string as the type for all the sets
  const sets: any[] = [
    'circled',
    'negative circled',
    'fullwidth',
    'math bold',
    'math bold fraktur',
    'math bold italic',
    'math bold script',
    'math double struck',
    'math mono',
    'math sans',
    'math sans bold',
    'math sans italic',
    'math sans bold italic',
    'parenthesized',
    'regional indicator',
    'squared',
    'negative squared'
  ];

  let message = '';
  for (const arg of args) {
    // Because messages can be multiline, if the argument includes a newline or is empty just add the arg and skip it
    if (arg.includes('\n') || arg === '') {
      message += arg;
      continue;
    }

    // Convert the current arg to a random set and add a space after it's done
    message += fancify({ set: sets[Math.floor(Math.random() * sets.length)], input: arg });
    message += ' ';
  }

  await msg.channel.send(message);
}
