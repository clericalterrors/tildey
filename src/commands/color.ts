/* istanbul ignore file */

import { createCanvas, Canvas } from 'canvas';
import { Attachment, GuildMember, Message, Role } from 'discord.js';
// @ts-ignore Ignore a declaration error from TS since is-color-stop doesn't have them
import { isColor } from 'is-color-stop';
import { getCustomEmoji, getGuildMember, getRandomColor, getRole, pluralize } from '../utils';

export async function colorCmd(msg: Message, name: string, args: string[]): Promise<void> {
  if (args.length === 0) {
    await msg.reply(`You forgot to include a color in your message!~ ${getCustomEmoji('tildey')}`);
    return;
  }

  const colors: string[] = [];
  for (const arg of args) {
    // Several ways of choosing a color:
    // - If a person is specified, get their display color
    // - If a role is specified, get the role's color
    // - If it's a valid CSS color
    // - If the argument is "random" then add a random color
    // - If the argument is "me" then add the author's display color
    if (msg.guild) {
      // We're trying to find a GuildMember or Role first because isColor
      // can wrongly identify Discord tags as valid colors
      const user: GuildMember | undefined = getGuildMember(msg, arg, true);
      if (user !== undefined) {
        colors.push(user.displayHexColor);
        continue;
      }

      const role: Role | undefined = getRole(msg, arg, true);
      if (role !== undefined) {
        colors.push(role.hexColor);
        continue;
      }
    }

    if (isColor(arg)) {
      colors.push(arg);
    } else if (arg.toLowerCase() === 'random') {
      colors.push(getRandomColor());
    } else if (arg.toLowerCase() === 'me') {
      colors.push(getGuildMember(msg, msg.author.id, true)!.displayHexColor);
    }
  }

  if (colors.length === 0) {
    await msg.reply(`I didn't find any valid ${name.toLowerCase()}s in your message!~ ${getCustomEmoji('pwease')}`);
    return;
  }

  const rectSide = 50;
  const maxColumns = 10;

  let canvas: Canvas = createCanvas(rectSide * colors.length, rectSide);

  if (colors.length >= maxColumns) {
    canvas = createCanvas(rectSide * maxColumns, Math.ceil(colors.length / maxColumns) * rectSide);
  }

  const context: CanvasRenderingContext2D = canvas.getContext('2d');

  for (const [i, element] of colors.entries()) {
    const x = rectSide * (i % maxColumns);
    const y = rectSide * Math.floor(i / maxColumns);
    context.fillStyle = element;
    context.fillRect(x, y, rectSide, rectSide);
  }

  let message = `Found ${pluralize(name.toLowerCase(), 's', colors.length)}: ` +
    `\`${colors.join('` `')}\`.`;

  if (message.length >= 2000) {
    message = `Found ${colors.length} ${name.toLowerCase()}s, too many to list here individually!~ :sweat_smile:`;
  }

  await msg.channel.send(message, new Attachment(canvas.toBuffer()));
}
