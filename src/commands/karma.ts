/* istanbul ignore file */

import { Message, User, RichEmbed } from 'discord.js';
import { EntityManager, getManager } from 'typeorm';
import { Karma } from '../db';
import { getUser, getCustomEmoji } from '../utils';

const karmaMessages: string[][] = [
  ['neutral', 'accepted', 'liked', 'idolized'],
  ['shunned', 'mixed', 'a smiling troublemaker', 'a good-natured rascal'],
  ['hated', 'a sneering punk', 'unpredictable', 'a dark hero'],
  ['vilified', 'a merciful thug', 'a soft-hearted devil', 'a wild child']
];

export async function karmaCmd(msg: Message, name: string, args: string[]): Promise<void> {
  let targetUser: User | undefined = msg.author;
  if (args[0]) {
    targetUser = getUser(msg, args[0]);
    if (targetUser === undefined) {
      return;
    }
  }

  const manager: EntityManager = getManager();
  let karma: Karma | undefined = await manager.findOne(
    Karma,
    {
      guild: msg.guild.id,
      user: targetUser.id
    }
  );
  if (karma === undefined) {
    karma = new Karma({
      exemplary: 0,
      guild: msg.guild.id,
      malice: 0,
      user: targetUser.id
    });
  }

  const exemplaryScore: number = karmaScoring(karma.exemplary);
  const maliceScore: number = karmaScoring(karma.malice);
  let karmaText = '';
  if (exemplaryScore >= 4 || maliceScore >= 4) {
    karmaText += '**a power user™**';
  } else {
    karmaText += karmaMessages[maliceScore][exemplaryScore];
  }

  const netKarma: number = karma.exemplary - karma.malice;
  const karmaEmbed: RichEmbed = new RichEmbed()
    .setThumbnail(targetUser.displayAvatarURL)
    .setTitle(`${targetUser.username}'s karma on ${msg.guild.name}`)
    .addField(getCustomEmoji('exemplary'), karma.exemplary, true)
    .addField(getCustomEmoji('malice'), karma.malice, true)
    .addField(`Total Karma: ${netKarma}`, `${targetUser.username} is seen as ${karmaText}.`, false)
    .setColor((netKarma < 0 ? '#dc322f' : '#278cd3'));

  await msg.channel.send(karmaEmbed);
  return Promise.resolve();
}

function karmaScoring(raw: number): number {
  if (raw <= 5) {
    return 0;
  }

  if (raw <= 20) {
    return 1;
  }

  if (raw <= 50) {
    return 2;
  }

  if (raw <= 199) {
    return 3;
  }

  return 4;
}
