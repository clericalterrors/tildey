/* istanbul ignore file */

import { Message, User } from 'discord.js';
import { EntityManager, getManager } from 'typeorm';
import { getCustomEmoji, getUser, getLastMessage } from '../utils';
import { Karma } from '../db';

export async function maliceCmd(msg: Message, name: string, args: string[]): Promise<void> {
  if (args.length === 0) {
    await msg.reply(`you forgot to include someone to Malice!~ ${getCustomEmoji('pwease')}`);
    return Promise.resolve();
  }

  const noDuplicates: User[] = [];
  for (const arg of args) {
    const user: User | undefined = getUser(msg, arg, true);
    if (user !== undefined && !noDuplicates.includes(user)) {
      noDuplicates.push(user);
    }
  }

  if (noDuplicates.length === 0) {
    await msg.reply('I couldn\'t find anyone by that name in this server!~ :shrug:');
    return Promise.resolve();
  }

  if (noDuplicates.some((user: User) => user.id === msg.author.id)) {
    await msg.reply(`you can't Malice yourself!~ ${getCustomEmoji('pwease')}`);
    return Promise.resolve();
  }

  const manager: EntityManager = getManager();
  for (const user of noDuplicates) {
    let karma: Karma | undefined = await manager.findOne(
      Karma,
      {
        guild: msg.guild.id,
        user: user.id
      }
    );
    if (karma === undefined) {
      karma = new Karma({
        exemplary: 0,
        guild: msg.guild.id,
        malice: 0,
        user: user.id
      });
    }

    karma.malice++;
    const lastMessage: Message | undefined = await getLastMessage(msg, user.id);
    if (lastMessage !== undefined) {
      try {
        await lastMessage.react(getCustomEmoji('malice'));
      } catch (error) {
        if (error.message.toLowerCase().includes('reaction blocked')) {
          await msg.react(getCustomEmoji('tildey'));
        }
      }
    }

    await manager.save(karma);
  }

  return Promise.resolve();
}
