/* istanbul ignore file */

import { Message, User } from 'discord.js';
import { getCustomEmoji, getLastMessage, log, getUser } from '../utils';

const allEmojis: string[] = [
  '💗',
  '💓',
  '💕',
  '💖',
  '💞',
  '💘',
  '💛',
  '💙',
  '💜',
  '💚',
  '💝',
  '💟',
  '💌'
];
const maxReactions = 3;

export async function loveCmd(msg: Message, name: string, args: string[]): Promise<void> {
  if (args.length === 0) {
    await msg.reply(`you forgot to include a @User in your message!~ ${getCustomEmoji('tildey')}`);
    return;
  }

  const noDuplicates: User[] = [];
  for (const arg of args) {
    const user: User | undefined = getUser(msg, arg, true);
    if (user !== undefined && !noDuplicates.includes(user)) {
      noDuplicates.push(user);
    }
  }

  if (noDuplicates.length === 0) {
    await msg.reply('I couldn\'t find anyone by that name in this server!~ :shrug:');
    return;
  }

  for (const user of noDuplicates) {
    let lastMessage: Message | undefined;
    try {
      lastMessage = await getLastMessage(msg, user.id);
    } catch (error) {
      log.error(`[LOVE] ${error}`);
    }

    if (lastMessage === undefined) {
      return;
    }

    // Get the already used reactions on the message
    let usedReactions: string[] = lastMessage.reactions.map((emoji) => emoji.emoji.toString());
    // Filter out any emojis that other people may have applied that aren't in our list
    usedReactions = usedReactions.filter((emoji) => allEmojis.includes(emoji));

    // Remove the emojis to only randomly pick from ones that weren't already used
    const emojis: string[] = allEmojis.filter((emoji) => !usedReactions.includes(emoji));

    for (let i = 0; i < maxReactions; i++) {
      let selectionIndex: number = Math.floor((Math.random() * emojis.length));
      let selection: string = emojis[selectionIndex];
      // Make sure no duplicate reactions can happen
      while (usedReactions.includes(selection)) {
        // Avoid infinite looping if all our emojis have been applied
        if (usedReactions.length >= emojis.length) {
          return;
        }

        selectionIndex = Math.floor((Math.random() * emojis.length));
        selection = emojis[selectionIndex];
      }

      // Return if there's no emojis to react with
      if (emojis.length === 0) {
        return;
      }

      emojis.splice(selectionIndex, 1);
      usedReactions.push(selection);
      if (lastMessage !== undefined) {
        try {
          await lastMessage.react(selection);
        } catch (error) {
          log.error(`[LOVE] ${error}`);
        }
      }
    }
  }
}
