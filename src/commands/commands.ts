import { resolve } from 'path';
import { Collection, Permissions } from 'discord.js';
import { readJSONSync } from 'fs-extra';
import { Command } from './command';

import { aboutCmd } from './about';
import { bigCmd } from './big';
import { chooseCmd } from './choose';
import { codepointsCmd } from './codepoints';
import { colorCmd } from './color';
import { configCmd } from './config';
import { debugCmd } from './debug';
import { duckduckgoCmd } from './duckduckgo';
import { exemplaryCmd } from './exemplary';
import { fancifyCmd } from './fancify';
import { feedbackCmd } from './feedback';
import { helpCmd } from './help';
import { karmaCmd } from './karma';
import { katCmd } from './kat';
import { lastFMCmd } from './lastfm';
import { listenBrainzCmd } from './listenbrainz';
import { loveCmd } from './love';
import { maliceCmd } from './malice';
import { owoCmd } from './owo';
import { pingPongCmd } from './ping-pong';
import { pollCmd } from './poll';
import { quickPollCmd } from './quick-poll';
import { quoteCmd } from './quote';
import { reminderCmd } from './reminder';
import { roleCmd } from './role';
import { spellCmd } from './spell';
import { spongeCmd } from './sponge';
import { superCmd } from './super';
import { tableFlipCmd } from './table-flip';
import { uptimeCmd } from './uptime';
import { whoisCmd } from './whois';
import { wolframAlphaCmd } from './wolfram-alpha';
import { whatisCmd } from './whatis';

export const Commands: Collection<string, Command> = new Collection();

// As the JSON can't store the commands' functions, we map them to their names here
const CommandExecutes: Collection<string, typeof Command.prototype.execute> = new Collection([
  ['about', aboutCmd],
  ['big', bigCmd],
  ['choose', chooseCmd],
  ['codepoints', codepointsCmd],
  ['color', colorCmd],
  ['configure', configCmd],
  ['duckduckgo', duckduckgoCmd],
  ['exemplary', exemplaryCmd],
  ['fancify', fancifyCmd],
  ['feedback', feedbackCmd],
  ['help', helpCmd],
  ['karma', karmaCmd],
  ['kat', katCmd],
  ['last fm', lastFMCmd],
  ['listenbrainz', listenBrainzCmd],
  ['love', loveCmd],
  ['malice', maliceCmd],
  ['owo', owoCmd],
  ['ping pong', pingPongCmd],
  ['poll', pollCmd],
  ['quote', quoteCmd],
  ['quick poll', quickPollCmd],
  ['reminder', reminderCmd],
  ['role', roleCmd],
  ['spell', spellCmd],
  ['sponge', spongeCmd],
  ['super', superCmd],
  ['table flip', tableFlipCmd],
  ['uptime', uptimeCmd],
  ['whatis', whatisCmd],
  ['whois', whoisCmd],
  ['wolfram|alpha', wolframAlphaCmd]
]);

export function initCommands(): void {
  const CommandsArray: Command[] = [];

  for (const command of readJSONSync(resolve('commands.json'))) {
    const newCommand: Command = new Command(command);
    // Since JSON doesn't store the classes we have to init them manually
    newCommand.authorPermissions = new Permissions(newCommand.authorPermissions);
    newCommand.clientPermissions = new Permissions(newCommand.clientPermissions);

    const cmdName = newCommand.name.toLowerCase();
    newCommand.execute = CommandExecutes.get(cmdName)!;

    CommandsArray.push(newCommand);
  }

  if (process.env.TLDY_ENV === 'development') {
    CommandsArray.push(new Command({
      name: 'Debug',
      description: 'Command to debug utils and other stuff',
      aliases: ['debug', 'dbg'],
      category: 'Special',
      args: [''],
      authorPermissions: new Permissions(['SEND_MESSAGES']),
      clientPermissions: new Permissions(['SEND_MESSAGES']),
      guildOnly: false,
      requiresDatabase: false,
      execute: debugCmd
    }));
  }

  CommandsArray.map((command) => command.aliases.map((alias) => Commands.set(alias, command)));
}
