/* istanbul ignore file */

import { Message, Channel } from 'discord.js';
import { getCustomEmoji, getUser, getGuildMember, getRole } from '../utils';

export async function whatisCmd(msg: Message, name: string, args: string[]): Promise<void> {
  if (args.length === 0) {
    await msg.reply(`you forgot to include an ID for me to find!~ ${getCustomEmoji('pwease')}`);
    return;
  }

  if (getGuildMember(msg, args[0], true) !== undefined) {
    await msg.reply(`that's a member of this server!~ ${getCustomEmoji('tildey')}`);
  } else if (getUser(msg, args[0], true) !== undefined) {
    await msg.reply(`that's a user but they're not in this server!~ ${getCustomEmoji('tildey')}`);
  } else if (getRole(msg, args[0], true) !== undefined) {
    await msg.reply(`that's a role!~ ${getCustomEmoji('tildey')}`);
  } else if (msg.client.emojis.has(args[0])) {
    await msg.reply(`that's a custom emoji!~ ${getCustomEmoji('tildey')}`);
  } else if (msg.client.channels.has(args[0])) {
    const channel: Channel = msg.client.channels.get(args[0])!;
    await msg.reply(`that's a ${channel.type} channel!~ ${getCustomEmoji('tildey')}`);
  } else {
    await msg.reply(`I couldn't find anything going by that ID!~ ${getCustomEmoji('pwease')}`);
  }

  return Promise.resolve();
}
