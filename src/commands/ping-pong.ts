/* istanbul ignore file */

import { Message } from 'discord.js';

export async function pingPongCmd(msg: Message, name: string): Promise<void> {
  // Calculate the time it took for Discord to receive the message and Tildey to get it (inbound)
  const initiated: number = Date.now();
  const inbound: number = initiated - msg.createdAt.getTime();

  // Send `:ping_pong: Ping` first and edit the message with the stats after sending
  const newMsg: Message = await msg.channel.send(`:ping_pong: ${name === 'ping' ? 'Pong' : 'Ping'}!~`) as Message;
  await appendStats(newMsg, initiated, inbound);
}

async function appendStats(msg: Message, initiated: number, inbound: number): Promise<void> {
  // Calculate the time it took for Tildey to send the message (outbound)
  const outbound: number = msg.createdAt.getTime() - initiated;
  // Edit the message to add `(received: Xms | responded: Yms | total: X+Yms)`
  const newMsg: string = `${msg.content}\n\`(received: ${inbound}ms | ` +
    `responded: ${outbound}ms | total: ${inbound + outbound}ms)\``;
  await msg.edit(newMsg);
}
