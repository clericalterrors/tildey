/* istanbul ignore file */

import querystring from 'querystring';
import { Message, RichEmbed } from 'discord.js';
import fecha from 'fecha';
import got, { Response } from 'got';
import { EntityManager, getManager } from 'typeorm';
import { ListenBrainz } from '../db';
import { getCustomEmoji, getPrefix } from '../utils';

export async function listenBrainzCmd(msg: Message, name: string, args: string[]): Promise<void> {
  if (process.env.TLDY_LISTENBRAINZ_KEY === undefined) {
    await msg.reply('that command is disabled!~ :no_entry_sign:');
    return;
  }

  const manager: EntityManager = getManager();
  let user: ListenBrainz | undefined = await manager.findOne(ListenBrainz, msg.author.id);
  if (args.length === 2 && args[0].toLowerCase() === 'set') {
    if (user === undefined) {
      user = new ListenBrainz({ id: msg.author.id });
    }

    user.username = args[1];
    await manager.save(user);
    await msg.react(getCustomEmoji('tildey'));
    return;
  }

  if (user === undefined || user.username === null) {
    await msg.reply(`you haven't set your ListenBrainz username yet. Use \`${getPrefix(msg)}${name} set <yourname>\` to get started!~ ${getCustomEmoji('tildey')}`);
    return;
  }

  if (args.length === 0) {
    msg.channel.startTyping();
    let res: Response<string> = await listenBrainzRequest(user.username, 'listens', { count: 2 });
    const listens: any = JSON.parse(res.body).payload;
    if (listens.count === 0) {
      await msg.reply(`I didn't find any recent listens! Is \`${user.username}\` correct? ListenBrainz's usernames are case-sensitive!~ ${getCustomEmoji('pwease')}`);
      msg.channel.stopTyping();
      return;
    }

    res = await listenBrainzRequest(user.username, 'playing-now');
    const nowPlaying: any = JSON.parse(res.body).payload;
    if (nowPlaying.count !== 0) {
      if (listens.count === 2) {
        listens.listens[1] = listens.listens[0];
        listens.listens[0] = nowPlaying.listens[0];
      }
    }

    const embed: RichEmbed = listenBrainzEmbed()
      .setFooter(`Recent — ${fecha.format(new Date(), 'YYYY-MM-DD HH:mm')}`);
    for (const [index, listen] of listens.listens.entries()) {
      const song = `${listen.track_metadata.artist_name} - ${listen.track_metadata.track_name}`;
      embed.addField(`${index === 0 ? 'Now Playing' : 'Played Previously'}`, `${song}`);
    }

    await msg.channel.send(embed);
    msg.channel.stopTyping();
  }
}

function listenBrainzEmbed(): RichEmbed {
  return new RichEmbed()
    .setTitle(`${getCustomEmoji('listenbrainz')} ListenBrainz`)
    .setColor(['#353070', '#eb743b'][Math.floor(Math.random() * 2)]);
}

async function listenBrainzRequest(username: string, endpoint: string, query: object = {}): Promise<Response<string>> {
  const url = `https://api.listenbrainz.org/1/user/${username}/${endpoint}?${querystring.stringify({ ...query })}`;
  return got(url, {
    headers: {
      Authorization: process.env.TLDY_LISTENBRAINZ_KEY,
      'User-Agent': process.env.TLDY_COMMON_AGENT
    }
  });
}
