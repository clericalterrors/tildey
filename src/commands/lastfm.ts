/* istanbul ignore file */

import querystring from 'querystring';
import { createCanvas, Canvas, Image, CanvasRenderingContext2D } from 'canvas';
import { Message, RichEmbed, Attachment } from 'discord.js';
import fecha from 'fecha';
import got from 'got';
import { EntityManager, getManager } from 'typeorm';
import { LastFM } from '../db';
import { getCustomEmoji, getPrefix, log } from '../utils';

export async function lastFMCmd(msg: Message, name: string, args: string[]): Promise<void> {
  if (process.env.TLDY_LASTFM_KEY === undefined) {
    await msg.reply('that command is disabled!~ :no_entry_sign:');
    return;
  }

  const manager: EntityManager = getManager();
  let user: LastFM | undefined = await manager.findOne(
    LastFM,
    {
      id: msg.author.id
    }
  );
  if (args.length === 2 && args[0].toLowerCase() === 'set') {
    if (user === undefined) {
      user = new LastFM({
        id: msg.author.id,
        username: args[1]
      });
    } else {
      user.username = args[1];
    }

    await manager.save(user);
    await msg.react(getCustomEmoji('tildey'));
    return;
  }

  if (user === undefined || user.username === null) {
    await msg.reply(`you haven't set your Last.fm username yet. Use \`${getPrefix(msg)}lastfm set <yourname>\` to get started!~ ${getCustomEmoji('tildey')}`);
    return;
  }

  if (args.length === 0) {
    let res: got.Response<string>;
    try {
      res = await lastFMRequest('user.getrecenttracks', {
        limit: 5,
        user: user.username
      });
    } catch (error) {
      // Error 17 means that the user's privacy setting prevent us from accessing their recent tracks
      if (JSON.parse(error.body).error === 17) {
        await msg.reply(`I can't look at your recent tracks because they're hidden. Check your privacy settings on Last.fm <https://last.fm/settings/privacy>!~ ${getCustomEmoji('pwease')}`);
        return;
      }

      if (error.statusCode === 404) {
        await msg.reply(`I didn't get any results for user \`${user.username}\`. Please make sure your username is set correctly!~ ${getCustomEmoji('pwease')}`);
        return;
      }

      throw error;
    }

    const data: any = JSON.parse(res.body).recenttracks;
    if (data['@attr'].total === '0') {
      await msg.reply(`it seems you don't have any recently played tracks!~ ${getCustomEmoji('pwease')}`);
      return;
    }

    const embed: RichEmbed = lastFMEmbed()
      .setThumbnail(data.track[0].image[data.track[0].image.length - 1]['#text'])
      .setFooter(`Recent — ${fecha.format(new Date(), 'YYYY-MM-DD HH:mm')}`);
    for (let i = 0; i < data.track.length; i++) {
      if (i === 5) {
        break;
      }

      const track = data.track[i];
      embed.addField(
        `Track 0${i + 1}`,
        `[${track.artist['#text']}](${track.url.slice(0, track.url.indexOf('/_/'))}) - ` +
        `[${track.name}](${track.url})\n`
      );
    }

    await msg.channel.send(embed);
    return;
  }

  if (args.length === 1) {
    const arg: string = args[0].toLowerCase();
    if (arg === 'profile') {
      let res: got.Response<string>;
      try {
        res = await lastFMRequest('user.getinfo', {
          user: user.username
        });
      } catch {
        await msg.reply(`Something went wrong downloading the info for user \`${user.username}\`. Please make sure your username is set correctly!~ ${getCustomEmoji('pwease')}`);
        return;
      }

      const data: any = JSON.parse(res.body).user;
      const thumbnail: string = data.image[data.image.length - 1]['#text'];
      const embed: RichEmbed = lastFMEmbed()
        .addField(
          'Profile',
          `Username: [${data.realname || data.name}](${data.url})` +
          (data.realname === '' ? '' : ` (${data.name})`) +
          (data.country === 'None' ? '' : `\nCountry: ${data.country}`) +
          (data.age === '0' ? '' : `\nAge: ${data.age}`)
        )
        .addField('Playcount', data.playcount, true)
        .addField('Playlists', data.playlists, true)
        .setThumbnail(thumbnail)
        .setFooter(`Profile — ${fecha.format(new Date(), 'YYYY-MM-DD HH:mm')}`);
      await msg.channel.send(embed);
    } else if (arg === 'weekly') {
      const weeklyDate: Date | null = user.latestWeekly;
      if (weeklyDate !== null) {
        const previous: Date = new Date(weeklyDate);
        const now: Date = new Date();
        if (
          previous.getUTCFullYear() === now.getUTCFullYear() &&
          previous.getUTCMonth() === now.getUTCMonth() &&
          previous.getUTCDate() === now.getUTCDate()
        ) {
          await msg.reply(`you can only request your weekly chart once a day!~ ${getCustomEmoji('tildey')}`);
          return;
        }
      }

      msg.channel.startTyping();
      let res: got.Response<string>;
      try {
        res = await lastFMRequest('user.getweeklyalbumchart', {
          user: user.username
        });
      } catch (error) {
        msg.channel.stopTyping();
        if (error.statusCode === 404) {
          await msg.reply(`I didn't get any results for user \`${user.username}\`. Please make sure your username is set correctly!~ ${getCustomEmoji('pwease')}`);
          return;
        }

        throw error;
      }

      const data: any = JSON.parse(res.body).weeklyalbumchart;
      if (data.album.length === 0) {
        msg.channel.stopTyping();
        await msg.reply(`I didn't get any albums from the Last.fm API, maybe you should listen to some!~ ${getCustomEmoji('pwease')}`);
        return;
      }

      const chartSize = 1000;
      const chartColumns = 3;
      const canvas: Canvas = createCanvas(chartSize, chartSize);
      const context: CanvasRenderingContext2D = canvas.getContext('2d');
      let drawnImages = 0;
      for (const album of data.album) {
        if (!album.mbid) {
          continue;
        }

        const albumRes: got.Response<string> = await lastFMRequest('album.getinfo', {
          mbid: album.mbid
        });
        await timeout(500);
        let albumData = JSON.parse(albumRes.body);
        if (albumData.error) {
          // Last.fm error "6" is "Album not found" which can potentially happen a lot
          if (albumData.error !== 6) {
            log.error(`[LASTFM] Error ${albumData.error}: ${albumData.message}`);
          }

          continue;
        }

        albumData = albumData.album;
        const albumCover: string = albumData.image[albumData.image.length - 1]['#text'];
        if (!albumCover) {
          continue;
        }

        await drawImage(context, albumCover, {
          dx: (chartSize / chartColumns) * (drawnImages % chartColumns),
          dy: (chartSize / chartColumns) * Math.floor(drawnImages / chartColumns),
          dw: chartSize / chartColumns,
          dh: chartSize / chartColumns
        });
        drawnImages++;
        if (drawnImages === 9) {
          break;
        }
      }

      user.latestWeekly = new Date();
      await manager.save(user);
      await msg.channel.send(new Attachment(canvas.toBuffer()));
      msg.channel.stopTyping();
    } else if (arg === 'loved') {
      let res: got.Response<string>;
      try {
        res = await lastFMRequest('user.getlovedtracks', {
          limit: 5,
          user: user.username
        });
      } catch (error) {
        if (error.statusCode === 404) {
          await msg.reply(`I didn't get any results for user \`${user.username}\`. Please make sure your username is set correctly!~ ${getCustomEmoji('pwease')}`);
          return;
        }

        throw error;
      }

      const data = JSON.parse(res.body).lovedtracks;
      if (data['@attr'].total === '0') {
        await msg.reply(`you have not loved any tracks yet!~ ${getCustomEmoji('pwease')}`);
        return;
      }

      const embed: RichEmbed = lastFMEmbed()
        .setThumbnail(data.track[0].image[data.track[0].image.length - 1]['#text'])
        .setFooter(`Loved — ${fecha.format(new Date(), 'YYYY-MM-DD HH:mm')}`);
      for (let i = 0; i < data.track.length; i++) {
        if (i === 5) {
          break;
        }

        const track = data.track[i];
        embed.addField(
          `Track 0${i + 1}`,
          `[${track.artist.name}](${track.artist.url}) - ` +
          `[${track.name}](${track.url})\n`
        );
      }

      await msg.channel.send(embed);
    } else if (arg === 'delete') {
      user.username = null;
      await manager.save(user);
      await msg.react(getCustomEmoji('tildey'));
    }
  }
}

function lastFMEmbed(): RichEmbed {
  return new RichEmbed()
    .setTitle(`${getCustomEmoji('lastfm')} Last.fm`)
    .setColor('#ba0000');
}

async function lastFMRequest(method: string, query: object): Promise<got.Response<string>> {
  const url = `https://ws.audioscrobbler.com/2.0/?${querystring.stringify({
    method,
    format: 'json',
    api_key: process.env.TLDY_LASTFM_KEY, // eslint-disable-line @typescript-eslint/camelcase
    ...query
  })}`;
  return got(url, {
    headers: {
      'User-Agent': process.env.TLDY_COMMON_AGENT
    }
  });
}

// Async timeout helper to limit how fast we make requests to Last.fm's API
async function timeout(time: number): Promise<void> {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, time);
  });
}

// Helper function to draw images async on the context because loading Images uses callbacks
async function drawImage(
  context: CanvasRenderingContext2D,
  url: string,
  coords: {dx: number; dy: number; dw: number; dh: number}
): Promise<void> {
  return new Promise((resolve, reject) => {
    const img: Image = new Image();
    // eslint-disable-next-line unicorn/prefer-add-event-listener
    img.onload = () => {
      context.drawImage(img, coords.dx, coords.dy, coords.dw, coords.dh);
      resolve();
    };

    // eslint-disable-next-line unicorn/prefer-add-event-listener
    img.onerror = (error) => {
      reject(error);
    };

    img.src = url;
  });
}
