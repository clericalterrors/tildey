/* istanbul ignore file */

import { join, resolve } from 'path';
import { Client, TextChannel, Collection, Message } from 'discord.js';
import { pathExists, readJSON, unlink } from 'fs-extra';
import express, { Express } from 'express';
import { DefaultLogFields } from 'simple-git/typings/response';
import { RestartMessage, Webhook } from '../interfaces';
import { CustomEmojis, log, getCustomEmoji, version, getLatestCommit, updatePresence, nextReminder } from '../utils';
import { initGitlabWebhook, initRestartWebhook } from '../webhooks';
import { initPrefixes } from '../utils/prefixes';

export async function clientReady(client: Client): Promise<void> {
  if (await pathExists(resolve('webhooks.json'))) {
    const webhooks: Express = express();
    webhooks.use(express.json());
    const webhookConfigs: Webhook[] = await readJSON(resolve('webhooks.json'));
    for (const webhook of webhookConfigs) {
      // Only enable webhooks that are marked as enabled
      if (!webhook.enabled) {
        log.debug(`[STARTUP] Webhook "${webhook.name}" with URL ${webhook.url} is disabled, not activating.`);
        continue;
      }

      // Initialize Gitlab webhooks
      if (webhook.type === 'gitlab') {
        if (client.channels.get(webhook.channel!) === undefined) {
          throw new Error(`Discord channel with ID ${webhook.channel} could not be found. Encountered while initializing webhook "${webhook.name}".`);
        }

        initGitlabWebhook(webhooks, client, webhook);
        const channel: TextChannel = client.channels.get(webhook.channel!) as TextChannel;
        log.debug(`[STARTUP] Webhook channel: ${channel.guild.name}#${channel.name}`);
        log.info(`[STARTUP] Listening for "${webhook.name}" GitLab Webhook updates at URL ${webhook.url}.`);
      } else if (webhook.type === 'restart') {
        initRestartWebhook(webhooks, webhook);
        log.info(`[STARTUP] Listening for "${webhook.name}" Restart Webhook updates at URL ${webhook.url}.`);
      }
    }

    webhooks.listen(process.env.TLDY_WEBHOOKS_PORT, () => {
      log.info(`[STARTUP] Webhooks listening on port ${process.env.TLDY_WEBHOOKS_PORT}`);
    });
  }

  // Find the custom emojis and set them in the collection
  const emojiGuild = client.guilds.find((guild) => guild.id === process.env.TLDY_EMOJIS_GUILD);
  for (const customEmoji of emojiGuild.emojis.array()) {
    CustomEmojis.set(customEmoji.name, customEmoji);
  }

  await updatePresence(client);
  log.debug(`[STARTUP] Found emoji guild "${emojiGuild.name}" with ${CustomEmojis.array().length} emojis`);
  log.info(`[STARTUP] ${client.user.tag} ready to work!~`);

  let restartMsgPath: string = join(resolve('temp'), 'restart_message.json');
  if (await pathExists(restartMsgPath)) {
    const data: RestartMessage = await readJSON(restartMsgPath);
    const msgs: Collection<string, Message> = await (client.channels.get(data.channelID) as TextChannel).fetchMessages();
    const commit: DefaultLogFields = await getLatestCommit();
    let newMessage = `<@${data.authorID}>, successfully restarted in ` +
      `${((Date.now() - new Date(data.initTime).getTime()) / 1000).toFixed(2)} ` +
      `seconds!~ ${getCustomEmoji('tildey')}`;

    if (data.version !== version && data.commit !== commit.hash.slice(0, 8)) {
      newMessage += `\nVersion ${data.version} → ${version} | Commit ${data.commit} → ${commit.hash.slice(0, 8)}`;
    } else if (data.version !== version) {
      newMessage += `\nVersion ${data.version} → ${version}`;
    } else if (data.commit !== commit.hash.slice(0, 8)) {
      newMessage += `\nCommit ${data.commit} → ${commit.hash.slice(0, 8)}`;
    }

    msgs.get(data.messageID)?.edit(newMessage);
    await unlink(restartMsgPath);
  }

  restartMsgPath = join(resolve('temp'), 'restart.json');
  if (await pathExists(restartMsgPath)) {
    await unlink(restartMsgPath);
  }

  await initPrefixes(client);
  await nextReminder(client, 0);
  return Promise.resolve();
}
