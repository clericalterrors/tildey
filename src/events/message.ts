/* istanbul ignore file */

import { Message } from 'discord.js';
import got from 'got';
import { getManager, EntityManager } from 'typeorm';
import { Commands } from '../commands';
import { Command } from '../commands/command';
import { db, GuildInfo } from '../db';
import { checkPermissions, getCustomEmoji, getGuildMember, getPrefix, log } from '../utils';

export async function clientMessage(msg: Message): Promise<void> {
  // Define some vars for easier access
  const { author, channel, client, content } = msg;
  const prefix: string = getPrefix(msg);

  // Return when the message…
  // - doesn't start with the prefix
  // - starts with the prefix twice
  // - starts with the prefix and an extra space
  // - is just the prefix
  // - the command entered starts with a number
  // - was sent by another bot
  if (!content.startsWith(prefix) ||
    content.startsWith(prefix + prefix) ||
    content.startsWith(prefix + ' ') ||
    content === prefix ||
    /^\d/.exec(content.slice(1)) ||
    author.bot) {
    // If the message contains the bot's ID and "prefix" then return the prefix
    if (!msg.author.bot && msg.content.includes(msg.client.user.id) && msg.content.toLowerCase().includes('prefix')) {
      await msg.reply(`my prefix here is \`${prefix}\`!~ ${getCustomEmoji('tildey')}`);
    // If someone mentioned the bot then react with :pingblob:
    } else if (new RegExp(`<@!?${msg.client.user.id}>`).exec(msg.content)) {
      await msg.react(getCustomEmoji('pingblob'));
    }

    return;
  }

  // Parse the content and separate the specified command and the extra arguments
  const args: string[] = content.slice(prefix.length).split(' ');
  const commandName: string = args.shift()!.toLowerCase();

  // If the passed through command isn't found, tell the user
  if (!Commands.has(commandName)) {
    await msg.reply(`I don't have any commands with that name!~ ${getCustomEmoji('pwease')}`);
    return;
  }

  // Because we know the command exists now, get it
  const command: Command = Commands.get(commandName)!;

  // Handle various cases where we wouldn't want to run a command:
  // - If it requires a database and the database isn't enabled
  if (!db.enabled && command.requiresDatabase) {
    await msg.reply('I can\'t run this command without MongoDB running, ask my owner to add it or turn it on!~ :no_entry_sign:');
    return;
  }

  // - If the command can only be done inside a guild and the channel isn't from a guild
  if (command.guildOnly && channel.type !== 'text') {
    await msg.reply('You need to be in a guild to use that command!~ :no_entry_sign:');
    return;
  }

  // - If we are in a guild channel, make sure the permissions are good and the command isn't disabled in that guild
  if (channel.type === 'text') {
    if (!checkPermissions(getGuildMember(msg, author.id, true)!.permissions, command.authorPermissions)) {
      await msg.reply('You don\'t have the permissions to use that command!~ :no_entry_sign:');
      return;
    }

    if (!checkPermissions(getGuildMember(msg, client.user.id, true)!.permissions, command.clientPermissions)) {
      await msg.reply('I don\'t have the permissions to use that command!~ :no_entry_sign:');
      return;
    }

    const manager: EntityManager = getManager();
    const guildInfo: GuildInfo | undefined = await manager.findOne(GuildInfo, msg.guild.id);
    if (guildInfo?.disabledCommands.includes(commandName)) {
      await msg.reply('that command has been disabled by a server administrator!~ :no_entry_sign:');
      return;
    }
  }

  // Try to execute the command, catching any unknown errors that might happen
  try {
    let logMsg: string = content;
    if (logMsg.length > 25) {
      logMsg = logMsg.slice(0, 25) + '…';
    }

    if (logMsg.indexOf('\n') > 1) {
      logMsg = logMsg.replace(/\n/g, '␊');
    }

    log.info(`[MESSAGE] ${author.tag} executed ${logMsg} in ${msg.guild ? msg.guild.name : 'a private message'}`);
    await command.execute(msg, commandName, args);
  } catch (error) {
    log.error(`[MESSAGE] ${error}`);
    await msg.reply('there was an error trying to execute that command!~ :sob:');

    if (
      process.env.TLDY_PUSHOVER_KEY === undefined ||
      process.env.TLDY_PUSHOVER_USER === undefined
    ) {
      return;
    }

    const response: got.Response<string> = await got(
      'https://api.pushover.net/1/messages.json', {
        body: {
          token: process.env.TLDY_PUSHOVER_KEY,
          user: process.env.TLDY_PUSHOVER_USER,
          message: `An error occurred:\n${error}`,
          timestamp: Date.now()
        },
        json: true,
        method: 'POST',
        throwHttpErrors: false
      }
    );
    if (response.statusCode === 200) {
      log.debug('[MESSAGE] Pushover notification successfully sent.');
    } else {
      log.warn(`[MESSAGE] Something went wrong sending the Pushover notification: ${response.statusCode} ${response.statusMessage}`);
    }
  }
}
