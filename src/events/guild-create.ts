/* istanbul ignore file */

import { Guild, Client } from 'discord.js';
import { log, updatePresence, prefixes } from '../utils';

export function clientGuildCreate(client: Client, guild: Guild): void {
  updatePresence(client);

  // When we join a new server, also init the default prefix if it doesn't already exist
  if (typeof prefixes.find((val) => val.guild === guild.id) === 'undefined') {
    prefixes.push({
      guild: guild.id,
      prefix: process.env.TLDY_PREFIX!
    });
  }

  log.info(`[JOINGUILD] Joined new guild "${guild.name}"`);
}
