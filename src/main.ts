/* istanbul ignore file */

import { Client, Guild, Message } from 'discord.js';
import { createConnection, getConnectionOptions, ConnectionOptions } from 'typeorm';
import { initCommands, Commands } from './commands';
import { dbOpen, dbError } from './db';
import { clientError, clientMessage, clientReady, clientGuildCreate, clientGuildDelete } from './events';
import { initEnv, initLog, log, version } from './utils';

function main(): void {
  initEnv('.env');
  initLog();
  initCommands();

  log.info(`[STARTUP] Starting Tildey v${version}`);
  log.debug(`[STARTUP] Default prefix: ${process.env.TLDY_PREFIX}`);
  log.debug(`[STARTUP] Enabled commands: ${Commands.array().map(({ name }) => name).join(', ')}`);

  // If the emojis guild var is left undefined, warn that the custom emojis won't work
  if (process.env.TLDY_EMOJIS_GUILD === undefined) {
    log.warn('[STARTUP] TLDY_EMOJIS_GUILD variable left as default, all custom emojis will be :white_large_square:');
  }

  getConnectionOptions().then((options: ConnectionOptions) => {
    createConnection({
      ...options,
      entities: [
        process.env.TLDY_ENV === 'development' ?
          'src/db/entities/*.ts' : 'dist/src/db/entities/*.js'
      ],
      migrations: [
        process.env.TLDY_ENV === 'development' ?
          'src/db/migrations/*.ts' : 'dist/src/db/migrations/*.js'
      ]
    }).then(async () => dbOpen(client))
      .catch(async (error: Error) => dbError(error, client));
  });

  // Create a new Discord client and add event handlers
  const client: Client = new Client();
  client.on('ready', () => {
    clientReady(client);
  });
  client.on('message', (msg: Message): void => {
    clientMessage(msg);
  });
  client.on('error', clientError);
  client.on('guildCreate', (guild: Guild) => clientGuildCreate(client, guild));
  client.on('guildDelete', (guild: Guild) => clientGuildDelete(client, guild));
}

main();
