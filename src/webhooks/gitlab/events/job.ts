import { RichEmbed, Emoji } from 'discord.js';
import fecha from 'fecha';
import { newGitlabEmbed, getAuthorLink } from '../shared';
import { getCustomEmoji } from '../../../utils';

export function formatJob(body: any): RichEmbed | undefined {
  let description = `Job [#${body.build_id}](${body.repository.homepage}/-/jobs/${body.build_id}) started by`;
  description += ` [${body.user.name}](${getAuthorLink(body)})`;
  const title = `${body.build_stage} — ${body.build_name}`;
  let jobDescription =
    `Started:  ${fecha.format(new Date(body.build_started_at), 'YYYY-MM-DD HH:mm:ss')}\n` +
    `Finished: ${fecha.format(new Date(body.build_finished_at), 'YYYY-MM-DD HH:mm:ss')}\n` +
    `Duration: ${Number(body.build_duration).toFixed(2)}s`;
  let thumbnail: string;
  let color: string;
  if (body.build_status === 'success') {
    thumbnail = (getCustomEmoji('vote_for') as Emoji).url;
    description += ' completed successfully.\n';
    color = '#50fa7b';
  } else if (body.build_status === 'failed') {
    thumbnail = (getCustomEmoji('vote_against') as Emoji).url;
    description += ' failed.\n';
    jobDescription += `\nFailure reason: \`${body.build_failure_reason}\``;

    if (body.build_allow_failure) {
      color = '#f1fa8c';
      jobDescription += '\nThis job is allowed to fail.';
    } else {
      color = '#ff5555';
    }
  } else {
    return undefined;
  }

  return newGitlabEmbed('Job', color, body)
    .addField(title, jobDescription)
    .setDescription(description)
    .setThumbnail(thumbnail);
}
