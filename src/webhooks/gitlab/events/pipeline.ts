import { RichEmbed, Emoji } from 'discord.js';
import { getCustomEmoji } from '../../../utils';
import { newGitlabEmbed, getAuthorLink } from '../shared';

export function formatPipeline(body: any): RichEmbed | undefined {
  const { id: pipelineID, status } = body.object_attributes;
  let description = `Pipeline [#${pipelineID}](${body.project.web_url}/pipelines/${pipelineID}) started by`;
  description += ` [${body.user.name}](${getAuthorLink(body)})`;
  let thumbnail: string;
  let color: string;
  if (status === 'success') {
    thumbnail = (getCustomEmoji('vote_for') as Emoji).url;
    description += ' completed successfully.\n';
    color = '#50fa7b';
  } else if (status === 'failed') {
    thumbnail = (getCustomEmoji('vote_against') as Emoji).url;
    description += ' failed.\n';
    color = '#ff5555';
  } else {
    return undefined;
  }

  const embed: RichEmbed = newGitlabEmbed('Pipeline', color, body)
    .setDescription(description)
    .setThumbnail(thumbnail);
  const statuses: string[] = [];
  for (const build of body.builds) {
    statuses.push(build.status);
    const { stage, name, id: buildID } = build;
    let statusEmoji: string | Emoji;
    let statusMessage: string;
    if (build.status === 'success') {
      statusEmoji = getCustomEmoji('vote_for');
      statusMessage = 'completed successfully.';
    } else if (build.status === 'failed') {
      statusEmoji = getCustomEmoji('vote_against');
      statusMessage = 'failed.';
    } else if (build.status === 'skipped') {
      statusEmoji = ':arrow_down:';
      statusMessage = 'skipped.';
    } else if (build.status === 'manual') {
      statusEmoji = ':gear:';
      statusMessage = 'requires manual activation.';
    } else {
      statusEmoji = ':question:';
      statusMessage = `unknown status: \`${build.status}\`.`;
    }

    embed.addField(
      `${stage} — ${name}`,
      `${statusEmoji} Job [#${buildID}](${body.project.web_url}/-/jobs/${buildID}) ${statusMessage}`
    );
  }

  if (statuses.every((val: string) =>
    val === 'success' ||
    val === 'failed' ||
    val === 'skipped' ||
    val === 'manual'
  )) {
    return embed;
  }

  return undefined;
}
