import { RichEmbed } from 'discord.js';
import { getAuthorLink, newGitlabEmbed } from '../shared';

export function formatNoteCommit(body: any): RichEmbed {
  const comment: string = `${body.object_attributes.note.slice(0, 80)}` +
    `${body.object_attributes.note.length > 80 ? '…' : ''}`;
  const seeMoreLink = `[See More…](${body.object_attributes.url})`;
  const description: string = `**[${body.user.name}](${getAuthorLink(body)}) commented on a commit:**\n` +
    `${comment}\n${seeMoreLink}`;

  return newGitlabEmbed('Comment on Commit', '#f1fa8c', body)
    .setDescription(description);
}
