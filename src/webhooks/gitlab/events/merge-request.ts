import { RichEmbed } from 'discord.js';
import { getAuthorLink, newGitlabEmbed } from '../shared';

export function formatMergeRequest(body: any): RichEmbed {
  let mergeRequestDescription: string = body.object_attributes.description === '' ?
    body.object_attributes.title :
    body.object_attributes.description;
  if (mergeRequestDescription.length > 1000) {
    mergeRequestDescription = mergeRequestDescription.slice(0, 1000) + '…';
  }

  const { iid, title } = body.object_attributes;
  const mergeRequestLink = `${body.project.web_url}/merge_requests/${iid}`;
  let { state } = body.object_attributes;
  /* istanbul ignore else */
  if (state !== 'merged' && state !== 'closed') {
    state = body.object_attributes.created_at === body.object_attributes.updated_at ?
      'opened' :
      'updated';
  }

  const sourceBranch: string = `[${body.object_attributes.source_branch}]` +
    `(${body.object_attributes.source.web_url}/tree/${body.object_attributes.source_branch})`;
  const targetBranch: string = `[${body.object_attributes.target_branch}]` +
    `(${body.object_attributes.target.web_url}/tree/${body.object_attributes.target_branch})`;

  const description: string = `[${body.user.name}](${getAuthorLink(body)}) ` +
    `${state} [!${iid}](${mergeRequestLink}). ` +
    `source: ${sourceBranch} | target: ${targetBranch}.`;

  return newGitlabEmbed('Merge Request', '#ff5555', body)
    .addField(title, mergeRequestDescription)
    .setDescription(description);
}
