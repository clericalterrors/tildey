/* istanbul ignore file */

import { resolve } from 'path';
import { readFile, writeJSON, ensureDir } from 'fs-extra';

async function main(): Promise<void> {
  const combined: string = await readFile(resolve('logs/combined.log'), 'UTF8');
  interface LineMeta {
    count: number;
    label: string;
  }
  interface LineCount {
    count: number;
    level: 'ERROR'|'WARN'|'INFO'|'DEBUG'|'SILLY';
    labels: LineMeta[];
  }

  const lineCounts: LineCount[] = [];
  const lines: string[] = combined.split('\n');
  for (const [i, line] of lines.entries()) {
    // Makes sure the line isn't empty and that it starts with the logger's date format
    if (line.length === 0 || !/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2},\d{3}/.exec(line)) {
      console.log(`Invalid line ${i + 1}:\n`);
      console.log(line);
      continue;
    }

    const dateString: string = line.slice(0, line.indexOf(' ', line.indexOf(' ') + 1));
    // Create a Date object from the dateString:
    // const date: Date = new Date(
    //   Number(dateString.slice(0, 4)),
    //   Number(dateString.slice(5, 7)),
    //   Number(dateString.slice(8, 10)),
    //   Number(dateString.slice(11, 13)),
    //   Number(dateString.slice(14, 16)),
    //   Number(dateString.slice(17, 19)),
    //   Number(dateString.slice(20, 23))
    // );
    const level: 'ERROR'|'WARN'|'INFO'|'DEBUG'|'SILLY' = line.slice(
      line.indexOf(dateString) + dateString.length + 1,
      line.indexOf(' ', line.indexOf(dateString) + dateString.length + 1)
    ) as any;
    const label: string = line.slice(line.indexOf('[') + 1, line.indexOf(']'));
    const count: LineCount | undefined = lineCounts.find((val) => val.level === level);
    if (typeof count === 'undefined') {
      lineCounts.push({
        count: 1,
        level,
        labels: [
          {
            count: 1,
            label
          }
        ]
      });
    } else {
      count.count++;
      const meta: LineMeta | undefined = count.labels.find((val) => val.label === label);
      if (typeof meta === 'undefined') {
        count.labels.push({
          count: 1,
          label
        });
      } else {
        meta.count++;
      }
    }
  }

  await ensureDir(resolve('temp'));
  await writeJSON('temp/linecounts.json', lineCounts, { spaces: 2 });
}

main();
