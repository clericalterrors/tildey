# Tildey Scripts

Self-contained scripts that can do various things. Run using `ts-node` or `node` after compiling, for example:

```sh
yarn ts-node src/scripts/log-report.ts
# Or
yarn tsc && node dist/src/scripts/log-report.js
```
