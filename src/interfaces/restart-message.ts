/* istanbul ignore file */

export interface RestartMessage {
  authorID: string;
  channelID: string;
  messageID: string;
  initTime: Date;
  version: string;
  commit: string;
}
