/* istanbul ignore file */

// Define any number of webhooks in "webhooks.json" example:
/*
[
  {
    "channel": "511725336946409472",
    "enabled": true,
    "name": "Tildey",
    "token": "very-secret",
    "type": "gitlab",
    "url": "/gitlab-wh"
  },
  {
    "channel": "511725336946409472",
    "enabled": false,
    "name": "Tildey Website",
    "token": "very-secret-but-disabled",
    "type": "gitlab",
    "url": "/gitlab-activity"
  }
]
*/

export interface Webhook {
  channel?: string;
  enabled: boolean;
  name: string;
  token: string;
  type: 'gitlab'|'restart';
  url: string;
}
