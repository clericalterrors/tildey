/* istanbul ignore file */

export interface Prefix {
  guild: string;
  prefix: string;
}
