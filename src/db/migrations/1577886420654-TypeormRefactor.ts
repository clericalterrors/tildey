/* istanbul ignore file */

import fs, { promises as fsp } from 'fs';
import { MigrationInterface, QueryRunner } from 'typeorm';
import { initEnv } from '../../utils';

/**
 * This migration is used to go from pre-2.0.0 when Tildey still used MongoDB.
 * To upgrade, create a `temp/` directory that includes all the relevant data as
 * JSON files. Before running this migration, make sure you have created the
 * TypeORM database (by default SQLite) and it has been synchronized.
 * This migration will only attempt to insert rows in the appropriate tables.
 * The data inside the JSON files should be an array of objects.
 * !! You cannot downgrade this migration, so if you already have an existing database
 * with data in it, make a backup! Since the only way to downgrade is to manually remove
 * everything in the migration or delete the database entirely. !!
 * The JSON files to create:
 * - guild-infos.json
 * - karmas.json
 * - lastfms.json
 * - quotes.json
 * - reminders.json
 * - whois-optouts.json
 */
export class version21577803486957 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    initEnv('.env');
    if (fs.existsSync('temp/guild-infos.json')) {
      const existing: any[] = JSON.parse(await fsp.readFile('temp/guild-infos.json', 'utf8'));
      let sqlQuery = 'insert into guild_info (id,prefix,"disabledCommands") values\n';
      for (const info of existing) {
        const prefix: string = info.prefix === undefined ? process.env.TLDY_PREFIX : info.prefix;
        const commands: string[] = info.disabledCommands === undefined ? [] : info.disabledCommands;
        sqlQuery += `(${info.guild},"${prefix}","${commands.join(',')}"),\n`;
      }

      sqlQuery = sqlQuery.slice(0, sqlQuery.length - 2) + ';';
      await queryRunner.query(sqlQuery);
    }

    if (fs.existsSync('temp/karmas.json')) {
      const existing: any[] = JSON.parse(await fsp.readFile('temp/karmas.json', 'utf8'));
      let sqlQuery = 'insert into karma (guild,user,exemplary,malice) values\n';
      for (const karma of existing) {
        for (const user of karma.karmas) {
          sqlQuery += `(${karma.guild},${user.user},${user.exemplary.$numberInt},${user.malice.$numberInt}),\n`;
        }
      }

      sqlQuery = sqlQuery.slice(0, sqlQuery.length - 2) + ';';
      await queryRunner.query(sqlQuery);
    }

    if (fs.existsSync('temp/lastfms.json')) {
      const existing: any[] = JSON.parse(await fsp.readFile('temp/lastfms.json', 'utf8'));
      let sqlQuery = 'insert into last_fm (id,username,latestWeekly) values\n';
      for (const lastfm of existing) {
        sqlQuery += `(${lastfm.discord},"${lastfm.lastfm}",NULL),\n`;
      }

      sqlQuery = sqlQuery.slice(0, sqlQuery.length - 2) + ';';
      await queryRunner.query(sqlQuery);
    }

    if (fs.existsSync('temp/quotes.json')) {
      const existing: any[] = JSON.parse(await fsp.readFile('temp/quotes.json', 'utf8'));
      let sqlQuery = 'insert into quote (guild,id,creator,author,content,color,date) values\n';
      const quotes: string[] = [];
      for (const guild of existing) {
        let id = 0;
        for (const quote of guild.quotes) {
          sqlQuery += `(${guild.guild},${id},"${quote.addedBy}","${quote.author}",`;
          sqlQuery += `?,"${quote.colour}","${quote.dateAdded.$date.$numberLong}"),\n`;
          quotes.push(quote.content);
          id++;
        }
      }

      sqlQuery = sqlQuery.slice(0, sqlQuery.length - 2) + ';';
      await queryRunner.query(sqlQuery, quotes);
    }

    if (fs.existsSync('temp/reminders.json')) {
      const existing: any[] = JSON.parse(await fsp.readFile('temp/reminders.json', 'utf8'));
      let sqlQuery = 'insert into reminder (channel,user,content,due) values\n';
      const reminders: string[] = [];
      for (const reminder of existing) {
        sqlQuery += `("${reminder.channel}","${reminder.user}",?,"${reminder.due.$date.$numberLong}"),\n`;
        reminders.push(reminder.text);
      }

      sqlQuery = sqlQuery.slice(0, sqlQuery.length - 2) + ';';
      await queryRunner.query(sqlQuery, reminders);
    }

    if (fs.existsSync('temp/whois-optouts.json')) {
      const existing: any[] = JSON.parse(await fsp.readFile('temp/whois-optouts.json', 'utf8'));
      let sqlQuery = 'insert into who_is (id) values\n';
      for (const optout of existing) {
        sqlQuery += `("${optout.user}"),\n`;
      }

      sqlQuery = sqlQuery.slice(0, sqlQuery.length - 2) + ';';
      await queryRunner.query(sqlQuery);
    }
  }

  public async down(): Promise<void> {
    throw new Error('This migration can\'t be reverted automatically as it would essentially mean deleting the entire database.');
  }
}
