/* istanbul ignore file */

import { MigrationInterface, QueryRunner } from 'typeorm';

export class LastFMUsernameNull1580910261736 implements MigrationInterface {
  name = 'LastFMUsernameNull1580910261736';

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query('CREATE TABLE "temporary_last_fm" ("id" varchar PRIMARY KEY NOT NULL, "username" varchar NOT NULL, "latestWeekly" date)', undefined);
    await queryRunner.query('INSERT INTO "temporary_last_fm"("id", "username", "latestWeekly") SELECT "id", "username", "latestWeekly" FROM "last_fm"', undefined);
    await queryRunner.query('DROP TABLE "last_fm"', undefined);
    await queryRunner.query('ALTER TABLE "temporary_last_fm" RENAME TO "last_fm"', undefined);
    await queryRunner.query('CREATE TABLE "temporary_last_fm" ("id" varchar PRIMARY KEY NOT NULL, "username" varchar, "latestWeekly" date)', undefined);
    await queryRunner.query('INSERT INTO "temporary_last_fm"("id", "username", "latestWeekly") SELECT "id", "username", "latestWeekly" FROM "last_fm"', undefined);
    await queryRunner.query('DROP TABLE "last_fm"', undefined);
    await queryRunner.query('ALTER TABLE "temporary_last_fm" RENAME TO "last_fm"', undefined);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query('ALTER TABLE "last_fm" RENAME TO "temporary_last_fm"', undefined);
    await queryRunner.query('CREATE TABLE "last_fm" ("id" varchar PRIMARY KEY NOT NULL, "username" varchar NOT NULL, "latestWeekly" date)', undefined);
    await queryRunner.query('INSERT INTO "last_fm"("id", "username", "latestWeekly") SELECT "id", "username", "latestWeekly" FROM "temporary_last_fm"', undefined);
    await queryRunner.query('DROP TABLE "temporary_last_fm"', undefined);
    await queryRunner.query('ALTER TABLE "last_fm" RENAME TO "temporary_last_fm"', undefined);
    await queryRunner.query('CREATE TABLE "last_fm" ("id" varchar PRIMARY KEY NOT NULL, "username" varchar NOT NULL, "latestWeekly" date)', undefined);
    await queryRunner.query('INSERT INTO "last_fm"("id", "username", "latestWeekly") SELECT "id", "username", "latestWeekly" FROM "temporary_last_fm"', undefined);
    await queryRunner.query('DROP TABLE "temporary_last_fm"', undefined);
  }
}
