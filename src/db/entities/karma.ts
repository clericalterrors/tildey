/* istanbul ignore file */

import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Karma {
  @PrimaryGeneratedColumn({
    comment: 'Internal unique ID.'
  })
  id: number;

  @Column({
    comment: 'The guild ID this karma belongs to.'
  })
  guild: string;

  @Column({
    comment: 'The user ID of the person this karma belongs to.'
  })
  user: string;

  @Column({
    comment: 'The amount of exemplaries received.',
    default: 0
  })
  exemplary: number;

  @Column({
    comment: 'The amount of malices received.',
    default: 0
  })
  malice: number;

  constructor(data: Partial<Karma>) {
    Object.assign(this, data);
  }
}
