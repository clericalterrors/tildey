/* istanbul ignore file */

import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Reminder {
  @PrimaryGeneratedColumn({
    comment: 'The ID of the reminder.'
  })
  id: number;

  @Column({
    comment: 'The channel ID of where to send the reminder.'
  })
  channel: string;

  @Column({
    comment: 'The user ID of the person that created the reminder/should be reminded.'
  })
  user: string;

  @Column({
    comment: 'The text to remind them of.'
  })
  content: string;

  @Column({
    comment: 'The date of when the reminder should be sent.'
  })
  due: Date;

  constructor(data: Partial<Reminder>) {
    Object.assign(this, data);
  }
}
