/* istanbul ignore file */

import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class GuildInfo {
  @PrimaryColumn({
    comment: 'The ID of the guild this info belongs to.'
  })
  id: string;

  @Column({
    comment: 'The prefix to use for this guild.'
  })
  prefix: string;

  @Column({
    comment: 'The list of disabled commands for this particular guild.',
    type: 'simple-array'
  })
  disabledCommands: string[];

  constructor(data: Partial<GuildInfo>) {
    Object.assign(this, data);
  }
}
