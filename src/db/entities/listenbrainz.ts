/* istanbul ignore file */

import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class ListenBrainz {
  @PrimaryColumn({
    comment: 'The user ID of the person this info belongs to.'
  })
  id: string;

  @Column({
    comment: 'The ListenBrainz username to use for this user.'
  })
  username: string;

  constructor(data: Partial<ListenBrainz>) {
    Object.assign(this, data);
  }
}
