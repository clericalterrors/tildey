/* istanbul ignore file */

import { Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class WhoIs {
  @PrimaryColumn({
    comment: 'The user ID of the person to opt-out of the whois command.'
  })
  id: string;

  constructor(data: Partial<WhoIs>) {
    Object.assign(this, data);
  }
}
