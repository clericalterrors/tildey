/* istanbul ignore file */

import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class LastFM {
  @PrimaryColumn({
    comment: 'The user ID of the person this info belongs to.'
  })
  id: string;

  @Column({
    comment: 'The LastFM username to use for this user.',
    nullable: true,
    type: 'varchar'
  })
  username: string | null;

  @Column({
    comment: 'The date of when this user used the weekly chart last, for rate-limiting purposes.',
    nullable: true,
    type: 'date'
  })
  latestWeekly: Date | null;

  constructor(data: Partial<LastFM>) {
    Object.assign(this, data);
  }
}
