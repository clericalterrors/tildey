/* istanbul ignore file */

import { Column, Entity, PrimaryGeneratedColumn, CreateDateColumn } from 'typeorm';

@Entity()
export class Quote {
  @PrimaryGeneratedColumn({
    comment: 'Interal/global ID, each quote will have a unique IID.'
  })
  iid: number;

  @Column({
    comment: 'The ID of the guild this was created in and belongs to.'
  })
  guild: string;

  @Column({
    comment: 'Local ID, no 2 quotes belonging to the same guild will have the same ID.'
  })
  id: number;

  @Column({
    comment: 'The user ID of the person that added this quote.'
  })
  creator: string;

  @Column({
    comment: 'The author of the quote.'
  })
  author: string;

  @Column({
    comment: 'The actual quote text, with a max length of 1950.',
    length: 1950
  })
  content: string;

  @Column({
    comment: 'The randomly generated color assigned to the quote.',
    length: 7
  })
  color: string;

  @CreateDateColumn({
    comment: 'The date when the quote was added.'
  })
  date: Date;

  constructor(data: Partial<Quote>) {
    Object.assign(this, data);
  }
}
