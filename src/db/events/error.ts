// Can't test this due to requiring access to the Discord API for login
/* istanbul ignore file */

import { Client } from 'discord.js';
import { log } from '../../utils';
import { db } from '..';

export async function dbError(error: Error, client: Client): Promise<void> {
  // TODO: This is probably wrong with the TypeORM refactor but I'm leaving it
  // for now.
  if (error.message.includes('Server selection timed out')) {
    db.enabled = false;
  } else {
    log.error(`[DB] ${error.message}`);
    log.error(`[DB] ${error.stack}`);
    return;
  }

  if (!db.enabled) {
    log.warn('[STARTUP] Database connection not established, certain commands will not work.');
    await client.login(process.env.TLDY_TOKEN);
  }
}
