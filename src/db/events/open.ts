// Can't test this due to requiring access to the Discord API for login
/* istanbul ignore file */

import { Client } from 'discord.js';
import { log } from '../../utils';
import { db } from '..';

export async function dbOpen(client: Client): Promise<void> {
  log.info('[STARTUP] Database connection established');
  db.enabled = true;
  await client.login(process.env.TLDY_TOKEN);
}
